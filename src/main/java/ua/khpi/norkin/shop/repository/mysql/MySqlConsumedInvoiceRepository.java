package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.ConsumedInvoice;
import ua.khpi.norkin.shop.model.ConsumedType;
import ua.khpi.norkin.shop.repository.ConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;

public class MySqlConsumedInvoiceRepository extends
		DatabaseAbstractRepository<ConsumedInvoice> implements
		ConsumedInvoiceRepository {

	private static final String FIND_ALL_CONSUMED_INVOICES = "SELECT * FROM shop.consumed_invoice;";
	private static final String FIND_CONSUMED_INVOICE = "SELECT * FROM shop.consumed_invoice WHERE consumed_invoice.id = ? LIMIT 1;";
	private static final String INSERT_CONSUMED_INVOICE = "INSERT INTO shop.consumed_invoice(consumed_invoice.date_consumed,consumed_invoice.consumed_type,consumed_invoice.is_consumed) VALUES (?,?,?);";
	private static final String UPDATE_CONSUMED_INVOICE = "UPDATE shop.consumed_invoice SET date_consumed=?,consumed_type=?,is_consumed=?,employee_id=? WHERE consumed_invoice.id= ? LIMIT 1;";

	private final static Logger LOG = Logger
			.getLogger(MySqlConsumedInvoiceRepository.class);

	public MySqlConsumedInvoiceRepository(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public void create(ConsumedInvoice entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_CONSUMED_INVOICE,
					PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setLong(counter++, entity.getDateConsumed());
			pstmt.setString(counter++, entity.getConsumedType().getValueEng());
			pstmt.setBoolean(counter++, entity.isConsumed());
//			pstmt.setLong(counter++, entity.getEmployeeId());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a consumed invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}
	}

	@Override
	public void update(ConsumedInvoice entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_CONSUMED_INVOICE);
			int counter = 1;
			pstmt.setLong(counter++, entity.getDateConsumed());
			pstmt.setString(counter++, entity.getConsumedType().getValueEng());
			pstmt.setBoolean(counter++, entity.isConsumed());
			pstmt.setLong(counter++, entity.getEmployeeId());

			pstmt.setLong(counter, entity.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a consumed invoice", e);
		} finally {
			close(connection);
			close(pstmt);
		}

	}

	@Override
	public ConsumedInvoice find(long entityPK) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ConsumedInvoice consumedInvoice = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_CONSUMED_INVOICE);
			pstmt.setLong(1, entityPK);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				consumedInvoice = null;
			} else {
				consumedInvoice = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a consumed invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return consumedInvoice;
	}

	@Override
	public Collection<ConsumedInvoice> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<ConsumedInvoice> consumedInvoices = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_CONSUMED_INVOICES);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				consumedInvoices.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all consumed invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return consumedInvoices;
	}

	private static ConsumedInvoice unmarshal(ResultSet rs) {
		ConsumedInvoice consumedInvoice = new ConsumedInvoice();
		try {
			consumedInvoice.setId(rs.getLong(Fields.ENTITY_ID));
			consumedInvoice.setDateConsumed(rs
					.getLong(Fields.CONSUMED_INVOICE_DATE_CONSUMED));
			consumedInvoice.setConsumedType(ConsumedType.valueOf(rs.getString(
					Fields.CONSUMED_INVOICE_TYPE).toUpperCase()));
			consumedInvoice.setEmployeeId(rs
					.getLong(Fields.CONSUMED_INVOICE_EMPLOYEE_ID));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to consumed invoice", e);
		}
		return consumedInvoice;
	}

}
