package ua.khpi.norkin.shop.repository.mysql.view;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.dbview.Warehouse;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.View;

public class WarehouseView extends DatabaseAbstractRepository<Warehouse> implements View<Warehouse> {

	private final static Logger LOG = Logger.getLogger(WarehouseView.class);

	public WarehouseView(DataSource dataSource) {
		super(dataSource);
	}

	private static final String FIND_ALL = "SELECT * FROM shop.warehouse;";
	private static final String FIND_BY_PRODUCT = "SELECT * FROM shop.warehouse WHERE product_id=? LIMIT 1;";

	@Override
	public Collection<Warehouse> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Warehouse> warehouses = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				warehouses.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all products in warehouse", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return warehouses;
	}

	@Override
	public Warehouse find(long productId) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Warehouse warehouse = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_BY_PRODUCT);
			pstmt.setLong(1, productId);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				warehouse = new Warehouse(0, 0, 0, BigDecimal.ZERO, BigDecimal.ZERO, 0);
			} else {
				warehouse = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a product in warehouse", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return warehouse;
	}

	private static Warehouse unmarshal(ResultSet rs) {
		Warehouse warehouse = new Warehouse();
		try {
			warehouse.setProductId(rs.getLong(Fields.WAREHOUSE_PRODUCT_ID));
			warehouse.setReceiptId(rs.getLong(Fields.WAREHOUSE_RECEIPT_ID));
			warehouse.setQuantityLeft(rs.getLong(Fields.WAREHOUSE_QUANTITY_LEFT));
			warehouse.setBuyPrice(rs.getBigDecimal(Fields.WAREHOUSE_BUY_PRICE));
			warehouse.setSellPrice(rs.getBigDecimal(Fields.WAREHOUSE_SELL_PRICE));
			warehouse.setDateReceipt(rs.getLong(Fields.WAREHOUSE_DATE_RECEIPT));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to warehouse", e);
			throw new RuntimeException(e);
		}
		return warehouse;
	}
}
