package ua.khpi.norkin.shop.repository;

import ua.khpi.norkin.shop.model.ConsumedInvoice;

public interface ConsumedInvoiceRepository extends Repository<ConsumedInvoice> {

}
