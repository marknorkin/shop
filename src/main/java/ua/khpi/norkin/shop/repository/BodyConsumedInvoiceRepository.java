package ua.khpi.norkin.shop.repository;

import java.util.Collection;

import ua.khpi.norkin.shop.model.BodyConsumedInvoice;

public interface BodyConsumedInvoiceRepository extends Repository<BodyConsumedInvoice> {

	public Collection<BodyConsumedInvoice> findByInvoiceId(long consumedInvoiceId);

}
