package ua.khpi.norkin.shop.repository;

import ua.khpi.norkin.shop.model.Brand;

public interface BrandRepository extends Repository<Brand> {

}
