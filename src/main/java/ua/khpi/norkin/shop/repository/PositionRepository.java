package ua.khpi.norkin.shop.repository;

import ua.khpi.norkin.shop.model.Position;

public interface PositionRepository extends Repository<Position> {

}
