package ua.khpi.norkin.shop.repository;

import java.util.Collection;

import ua.khpi.norkin.shop.model.BodyReceiptInvoice;

public interface BodyReceiptInvoiceRepository extends Repository<BodyReceiptInvoice> {

	public Collection<BodyReceiptInvoice> findByReceiptInvoiceId(long receiptInvoiceId);
}
