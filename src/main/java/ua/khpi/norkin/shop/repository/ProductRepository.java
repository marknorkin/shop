package ua.khpi.norkin.shop.repository;

import java.util.Collection;

import ua.khpi.norkin.shop.model.Product;

public interface ProductRepository extends Repository<Product> {

	/**
	 * @return total number of product records in repository
	 */
	public long recordsCount();

	/**
	 * Finds specified products amount for specified page number.
	 *
	 * @param page
	 *            - for which products should be found
	 * @param howMany
	 *            - size of returned list
	 * @return list of products for given page
	 */
	public Collection<Product> findProducts(long page, int howMany);

	public Product find(String name);
}
