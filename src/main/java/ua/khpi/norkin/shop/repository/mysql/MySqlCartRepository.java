package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Cart;
import ua.khpi.norkin.shop.repository.CartRepository;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;

public class MySqlCartRepository extends DatabaseAbstractRepository<Cart> implements CartRepository {

	private static final String FIND_ALL_CARTS = "SELECT * FROM shop.cart;";
	private static final String FIND_CART = "SELECT * FROM shop.cart WHERE cart.id = ? LIMIT 1;";
	private static final String FIND_CARTS_BY_ORDER = "SELECT * FROM shop.cart WHERE cart.order_id=? ;";
	private static final String INSERT_CART = "INSERT INTO shop.cart(cart.product_id,cart.order_id,cart.quantity, cart.receipt_invoice_id) VALUES (?,?,?,?);";
	private static final String UPDATE_CART = "UPDATE shop.cart SET product_id=?,order_id=?,quantity=?, receipt_invoice_id=? WHERE cart.id= ? LIMIT 1;";

	private final static Logger LOG = Logger.getLogger(MySqlCartRepository.class);

	public MySqlCartRepository(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public void create(Cart entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {

			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_CART, PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setLong(counter++, entity.getProductId());
			pstmt.setLong(counter++, entity.getOrderId());
			pstmt.setLong(counter++, entity.getQuantity());
			pstmt.setLong(counter++, entity.getReceiptInvoiceId());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a cart", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}
	}

	@Override
	public void update(Cart entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_CART);
			int counter = 1;
			pstmt.setLong(counter++, entity.getProductId());
			pstmt.setLong(counter++, entity.getOrderId());
			pstmt.setLong(counter++, entity.getQuantity());
			pstmt.setLong(counter++, entity.getReceiptInvoiceId());

			pstmt.setLong(counter, entity.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a cart", e);
		} finally {
			close(connection);
			close(pstmt);
		}

	}

	@Override
	public Cart find(long entityPK) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Cart cart = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_CART);
			pstmt.setLong(1, entityPK);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				cart = null;
			} else {
				cart = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a cart", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return cart;
	}

	@Override
	public Collection<Cart> findByOrder(long orderId) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Cart> carts = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_CARTS_BY_ORDER);
			pstmt.setLong(1, orderId);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				carts.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find carts by order", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return carts;
	}

	@Override
	public Collection<Cart> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Cart> carts = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_CARTS);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				carts.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all carts", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return carts;
	}

	private static Cart unmarshal(ResultSet rs) {
		Cart cart = new Cart();
		try {
			cart.setId(rs.getLong(Fields.ENTITY_ID));
			cart.setProductId(rs.getLong(Fields.CART_PRODUCT_ID));
			cart.setOrderId(rs.getLong(Fields.CART_ORDER_ID));
			cart.setQuantity(rs.getLong(Fields.CART_QUANTITY));
			cart.setReceiptInvoiceId(rs.getLong(Fields.CART_RECEIPT_INVOICE_ID));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to cart", e);
		}
		return cart;
	}
}
