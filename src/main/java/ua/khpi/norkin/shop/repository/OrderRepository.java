package ua.khpi.norkin.shop.repository;

import java.util.Collection;

import ua.khpi.norkin.shop.model.Order;

public interface OrderRepository extends Repository<Order> {

	public Collection<Order> findMyOrders(long clientId);

	public Collection<Order> findOrdersInProgress();
}
