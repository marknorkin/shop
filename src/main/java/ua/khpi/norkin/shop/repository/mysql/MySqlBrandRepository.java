package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Brand;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;

public class MySqlBrandRepository extends DatabaseAbstractRepository<Brand>
		implements BrandRepository {

	private static final String FIND_ALL_BRANDS = "SELECT * FROM shop.brand;";
	private static final String FIND_BRAND = "SELECT * FROM shop.brand WHERE brand.id = ? LIMIT 1;";
	private static final String INSERT_BRAND = "INSERT INTO shop.brand(brand.name) VALUES (?);";
	private static final String UPDATE_BRAND = "UPDATE shop.brand SET name=? WHERE brand.id= ? LIMIT 1;";

	private final static Logger LOG = Logger
			.getLogger(MySqlBrandRepository.class);
	public MySqlBrandRepository(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public void create(Brand entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {

			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_BRAND,
					PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setString(counter++, entity.getName());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a brand", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}
	}

	@Override
	public void update(Brand entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_BRAND);
			int counter = 1;
			pstmt.setString(counter++, entity.getName());

			pstmt.setLong(counter, entity.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a brand", e);
		} finally {
			close(connection);
			close(pstmt);
		}

	}

	@Override
	public Brand find(long entityPK) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Brand brand = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_BRAND);
			pstmt.setLong(1, entityPK);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				brand = null;
			} else {
				brand = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a brand", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return brand;
	}

	@Override
	public Collection<Brand> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Brand> brands = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_BRANDS);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				brands.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all brands", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return brands;
	}

	private static Brand unmarshal(ResultSet rs) {
		Brand brand = new Brand();
		try {
			brand.setId(rs.getLong(Fields.ENTITY_ID));
			brand.setName(rs.getString(Fields.BRAND_NAME));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to brand", e);
		}
		return brand;
	}

}
