package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Order;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.OrderRepository;

public class MySqlOrderRepository extends DatabaseAbstractRepository<Order> implements OrderRepository {

	private static final String FIND_ALL_ORDERS = "SELECT * FROM shop.order;";
	private static final String FIND_MY_ORDERS = "SELECT * FROM shop.order WHERE order.client_id=?;";
	private static final String FIND_ORDERS_IN_PROGRESS = "SELECT * FROM shop.order WHERE order.date_send IS NULL OR order.date_payment IS NULL OR order.employee_id IS NULL;";
	private static final String FIND_ORDER = "SELECT * FROM shop.order WHERE order.id = ? LIMIT 1;";
	private static final String INSERT_ORDER = "INSERT INTO shop.order(order.client_id,order.date_order,order.sum, order.consumed_invoice_id) VALUES (?,?,?,?);";
	private static final String UPDATE_ORDER = "UPDATE shop.order SET client_id=?,date_order=?,date_send=?,date_payment=?,sum=?,id_express_invoice=?,consumed_invoice_id=?,employee_id=? WHERE order.id= ? LIMIT 1;";

	private final static Logger LOG = Logger.getLogger(MySqlOrderRepository.class);

	public MySqlOrderRepository(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public void create(Order entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {

			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_ORDER, PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setLong(counter++, entity.getClientId());
			pstmt.setLong(counter++, entity.getDateOrder());
			// pstmt.setLong(counter++, entity.getDateSend());
			// pstmt.setLong(counter++, entity.getDatePayment());
			pstmt.setBigDecimal(counter++, entity.getSum());
			// pstmt.setLong(counter++, entity.getExpressInvoiceId());
			pstmt.setLong(counter++, entity.getConsumedInvoiceId());
			// pstmt.setLong(counter, entity.getEmployeeId());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create an order", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}
	}

	@Override
	public void update(Order entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_ORDER);
			int counter = 1;
			pstmt.setLong(counter++, entity.getClientId());
			pstmt.setLong(counter++, entity.getDateOrder());

			if (entity.getDateSend() == 0 && entity.getDatePayment() == 0) {
				pstmt.setNull(counter++, Types.BIGINT);
				pstmt.setNull(counter++, Types.BIGINT);
			} else {
				pstmt.setLong(counter++, entity.getDateSend());
				pstmt.setLong(counter++, entity.getDatePayment());
			}

			pstmt.setBigDecimal(counter++, entity.getSum());
			pstmt.setLong(counter++, entity.getExpressInvoiceId());
			pstmt.setLong(counter++, entity.getConsumedInvoiceId());

			if (entity.getEmployeeId() == 0) {
				pstmt.setNull(counter++, Types.BIGINT);
			} else {
				pstmt.setLong(counter++, entity.getEmployeeId());
			}

			pstmt.setLong(counter, entity.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update an order", e);
		} finally {
			close(connection);
			close(pstmt);
		}
	}

	@Override
	public Order find(long entityPK) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Order order = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ORDER);
			pstmt.setLong(1, entityPK);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				order = null;
			} else {
				order = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find an order", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return order;
	}

	@Override
	public Collection<Order> findMyOrders(long clientId) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_MY_ORDERS);
			pstmt.setLong(1, clientId);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				orders.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find my orders", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return orders;
	}

	@Override
	public Collection<Order> findOrdersInProgress() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ORDERS_IN_PROGRESS);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				orders.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find orders in progress", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return orders;
	}

	@Override
	public Collection<Order> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_ORDERS);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				orders.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all orders", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return orders;
	}

	private static Order unmarshal(ResultSet rs) {
		Order order = new Order();
		try {
			order.setId(rs.getLong(Fields.ENTITY_ID));
			order.setClientId(rs.getLong(Fields.ORDER_CLIENT_ID));
			order.setDateOrder(rs.getLong(Fields.ORDER_DATE_ORDER));
			order.setDateSend(rs.getLong(Fields.ORDER_DATE_SEND));
			order.setDatePayment(rs.getLong(Fields.ORDER_DATE_PAYMENT));
			order.setSum(rs.getBigDecimal(Fields.ORDER_SUM));
			order.setExpressInvoiceId(rs.getLong(Fields.ORDER_EXPRESS_INVOICE_ID));
			order.setConsumedInvoiceId(rs.getLong(Fields.ORDER_CONSUMED_INVOICE_ID));
			order.setEmployeeId(rs.getLong(Fields.ORDER_EMPLOYEE_ID));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to order", e);
		}
		return order;
	}
}
