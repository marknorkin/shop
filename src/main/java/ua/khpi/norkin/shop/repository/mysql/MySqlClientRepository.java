package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Client;
import ua.khpi.norkin.shop.repository.ClientRepository;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;

/**
 * Client DAO object. Performs basic read/write operations on Client data.
 *
 * @author Mark Norkin
 *
 */
public class MySqlClientRepository extends DatabaseAbstractRepository<Client>
		implements ClientRepository {

	private static final String FIND_ALL_CLIENTS = "SELECT * FROM shop.client;";
	private static final String FIND_CLIENT = "SELECT * FROM shop.client WHERE client.id = ? LIMIT 1;";
	private static final String FIND_CLIENT_BY_EMAIL_AND_PASS = "SELECT * FROM shop.client WHERE client.email = ? AND client.password=? LIMIT 1;";
	private static final String FIND_CLIENT_BY_EMAIL = "SELECT * FROM shop.client WHERE client.email = ? LIMIT 1;";
	private static final String INSERT_CLIENT = "INSERT INTO shop.client(client.first_name,client.last_name,client.email,client.password,client.city,client.street,client.house,client.appartement,client.phone,client.is_active) VALUES (?,?,?,?,?,?,?,?,?,?);";
	private static final String UPDATE_CLIENT = "UPDATE shop.client SET first_name=?,last_name=?,email=?,password=?,city=?,street=?,house=?,appartement=?,phone=?, is_active=? WHERE client.id= ? LIMIT 1;";

	private final static Logger LOG = Logger
			.getLogger(MySqlClientRepository.class);

	public MySqlClientRepository(DataSource dataSource) {
		super(dataSource);
	}

	public void create(Client client) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {

			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_CLIENT,
					PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setString(counter++, client.getFirstName());
			pstmt.setString(counter++, client.getLastName());
			pstmt.setString(counter++, client.getEmail());
			pstmt.setString(counter++, client.getPassword());
			pstmt.setString(counter++, client.getCity());
			pstmt.setString(counter++, client.getStreet());
			pstmt.setString(counter++, client.getHouse());
			pstmt.setString(counter++, client.getAppartement());
			pstmt.setString(counter++, client.getPhone());
			pstmt.setBoolean(counter, client.getActiveStatus());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				client.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a client", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}
	}

	public void update(Client client) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_CLIENT);
			int counter = 1;
			pstmt.setString(counter++, client.getFirstName());
			pstmt.setString(counter++, client.getLastName());
			pstmt.setString(counter++, client.getEmail());
			pstmt.setString(counter++, client.getPassword());
			pstmt.setString(counter++, client.getCity());
			pstmt.setString(counter++, client.getStreet());
			pstmt.setString(counter++, client.getHouse());
			pstmt.setString(counter++, client.getAppartement());
			pstmt.setString(counter++, client.getPhone());
			pstmt.setBoolean(counter++, client.getActiveStatus());

			pstmt.setLong(counter, client.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a client", e);
		} finally {
			close(connection);
			close(pstmt);
		}
	}

	public Client find(long clientId) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Client client = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_CLIENT);
			pstmt.setLong(1, clientId);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				client = null;
			} else {
				client = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a client", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return client;
	}

	@Override
	public Client find(String email, String password) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Client client = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_CLIENT_BY_EMAIL_AND_PASS);
			pstmt.setString(1, email);
			pstmt.setString(2, password);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				client = null;
			} else {
				client = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a client", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return client;
	}

	@Override
	public Client find(String email) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Client client = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_CLIENT_BY_EMAIL);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				client = null;
			} else {
				client = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a client", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return client;
	}

	public List<Client> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Client> clients = new ArrayList<Client>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_CLIENTS);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				clients.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all clients", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return clients;
	}

	/**
	 * Unmarshals Client record in database to Java instance.
	 *
	 * @param rs
	 *            - record from result set
	 * @return Client instance of database record.
	 */
	private static Client unmarshal(ResultSet rs) {
		Client client = new Client();
		try {
			client.setId(rs.getLong(Fields.ENTITY_ID));
			client.setFirstName(rs.getString(Fields.CLIENT_FIRST_NAME));
			client.setLastName(rs.getString(Fields.CLIENT_LAST_NAME));
			client.setEmail(rs.getString(Fields.CLIENT_EMAIL));
			client.setPassword(rs.getString(Fields.CLIENT_PASSWORD));
			client.setCity(rs.getString(Fields.CLIENT_CITY));
			client.setStreet(rs.getString(Fields.CLIENT_STREET));
			client.setHouse(rs.getString(Fields.CLIENT_HOUSE));
			client.setAppartement(rs.getString(Fields.CLIENT_APPARTEMENT));
			client.setPhone(rs.getString(Fields.CLIENT_PHONE));
			client.setActiveStatus(rs.getBoolean(Fields.CLIENT_ACTIVE_STATUS));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to client", e);
		}
		return client;
	}
}
