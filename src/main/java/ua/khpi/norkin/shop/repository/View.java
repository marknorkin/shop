package ua.khpi.norkin.shop.repository;


public interface View<T> extends Repository<T> {

	@Override
	default public void create(T entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	default public void update(T entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	default public void delete(T entity) {
		throw new UnsupportedOperationException();
	}
}
