package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.BodyConsumedInvoice;
import ua.khpi.norkin.shop.repository.BodyConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;

public class MySqlBodyConsumedInvoice extends DatabaseAbstractRepository<BodyConsumedInvoice> implements
		BodyConsumedInvoiceRepository {

	private static final String FIND_ALL_BODY_CONSUMED_INVOICES = "SELECT * FROM shop.body_consumed_invoice;";
	private static final String FIND_BODY_CONSUMED_INVOICE = "SELECT * FROM shop.body_consumed_invoice WHERE body_consumed_invoice.id = ? LIMIT 1;";
	private static final String FIND_BODIES_BY_INVOICE = "SELECT * FROM shop.body_consumed_invoice WHERE body_consumed_invoice.consumed_invoice_id = ?;";
	private static final String INSERT_BODY_CONSUMED_INVOICE = "INSERT INTO shop.body_consumed_invoice(body_consumed_invoice.product_id, body_consumed_invoice.consumed_invoice_id, body_consumed_invoice.quantity, body_consumed_invoice.receipt_invoice_id) VALUES (?,?,?,?);";
	private static final String UPDATE_BODY_CONSUMED_INVOICE = "UPDATE shop.body_consumed_invoice SET product_id=?, consumed_invoice_id=?, quantity=?, receipt_invoice_id=? WHERE body_consumed_invoice.id= ? LIMIT 1;";

	private final static Logger LOG = Logger.getLogger(MySqlBodyConsumedInvoice.class);

	public MySqlBodyConsumedInvoice(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public void create(BodyConsumedInvoice entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_BODY_CONSUMED_INVOICE, PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setLong(counter++, entity.getProductId());
			pstmt.setLong(counter++, entity.getConsumedInvoiceId());
			pstmt.setLong(counter++, entity.getQuantity());
			pstmt.setLong(counter++, entity.getReceiptInvoiceId());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a body consumed invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}

	}

	@Override
	public void update(BodyConsumedInvoice entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_BODY_CONSUMED_INVOICE);
			int counter = 1;
			pstmt.setLong(counter++, entity.getProductId());
			pstmt.setLong(counter++, entity.getConsumedInvoiceId());
			pstmt.setLong(counter++, entity.getQuantity());
			pstmt.setLong(counter++, entity.getReceiptInvoiceId());

			pstmt.setLong(counter, entity.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a body consumed invoice", e);
		} finally {
			close(connection);
			close(pstmt);
		}
	}

	@Override
	public BodyConsumedInvoice find(long entityPK) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BodyConsumedInvoice bodyConsumedInvoice = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_BODY_CONSUMED_INVOICE);
			pstmt.setLong(1, entityPK);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				bodyConsumedInvoice = null;
			} else {
				bodyConsumedInvoice = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a body consumed invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return bodyConsumedInvoice;
	}

	@Override
	public Collection<BodyConsumedInvoice> findByInvoiceId(long consumedInvoiceId) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<BodyConsumedInvoice> bodyConsumedInvoices = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_BODIES_BY_INVOICE);
			pstmt.setLong(1, consumedInvoiceId);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				bodyConsumedInvoices.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all body consumed invoice by consumed invoice ID", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return bodyConsumedInvoices;
	}

	@Override
	public Collection<BodyConsumedInvoice> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<BodyConsumedInvoice> bodyConsumedInvoices = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_BODY_CONSUMED_INVOICES);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				bodyConsumedInvoices.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all body consumed invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return bodyConsumedInvoices;
	}

	private static BodyConsumedInvoice unmarshal(ResultSet rs) {
		BodyConsumedInvoice bodyConsumedInvoice = new BodyConsumedInvoice();
		try {
			bodyConsumedInvoice.setId(rs.getLong(Fields.ENTITY_ID));
			bodyConsumedInvoice.setProductId(rs.getLong(Fields.BODY_CONSUMED_INVOICE_PRODUCT_ID));
			bodyConsumedInvoice.setConsumedInvoiceId(rs.getLong(Fields.BODY_CONSUMED_INVOICE_CONSUMED_INVOICE_ID));
			bodyConsumedInvoice.setQuantity(rs.getLong(Fields.BODY_CONSUMED_INVOICE_QUANTITY));
			bodyConsumedInvoice.setReceiptInvoiceId(rs.getLong(Fields.BODY_CONSUMED_INVOICE_RECEIPT_INVOICE_ID));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to body consumed invoice", e);
		}
		return bodyConsumedInvoice;
	}

}
