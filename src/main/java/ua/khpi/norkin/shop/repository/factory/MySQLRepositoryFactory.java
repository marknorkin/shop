package ua.khpi.norkin.shop.repository.factory;

import ua.khpi.norkin.shop.model.dbview.Warehouse;
import ua.khpi.norkin.shop.repository.BodyConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.BodyReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.CartRepository;
import ua.khpi.norkin.shop.repository.ConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.EmployeeRepository;
import ua.khpi.norkin.shop.repository.OrderRepository;
import ua.khpi.norkin.shop.repository.PositionRepository;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.ReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.View;
import ua.khpi.norkin.shop.repository.datasource.DataSourceFactory;
import ua.khpi.norkin.shop.repository.datasource.DataSourceType;
import ua.khpi.norkin.shop.repository.mysql.MySqlBodyConsumedInvoice;
import ua.khpi.norkin.shop.repository.mysql.MySqlBodyReceiptInvoice;
import ua.khpi.norkin.shop.repository.mysql.MySqlBrandRepository;
import ua.khpi.norkin.shop.repository.mysql.MySqlCartRepository;
import ua.khpi.norkin.shop.repository.mysql.MySqlClientRepository;
import ua.khpi.norkin.shop.repository.mysql.MySqlConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.mysql.MySqlEmployeeRepository;
import ua.khpi.norkin.shop.repository.mysql.MySqlOrderRepository;
import ua.khpi.norkin.shop.repository.mysql.MySqlPositionRepository;
import ua.khpi.norkin.shop.repository.mysql.MySqlProductRepository;
import ua.khpi.norkin.shop.repository.mysql.MySqlReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.mysql.view.WarehouseView;

public class MySQLRepositoryFactory extends RepositoryFactory {

	private static final MySQLRepositoryFactory INSTANCE = new MySQLRepositoryFactory();

	private final MySqlClientRepository CLIENT_REPOSITORY = new MySqlClientRepository(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlProductRepository PRODUCT_REPOSITORY = new MySqlProductRepository(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlEmployeeRepository EMPLOYEE_REPOSITORY = new MySqlEmployeeRepository(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlOrderRepository ORDER_REPOSITORY = new MySqlOrderRepository(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlCartRepository CART_REPOSITORY = new MySqlCartRepository(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlReceiptInvoiceRepository RECEIPT_INVOICE_REPOSITORY = new MySqlReceiptInvoiceRepository(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlConsumedInvoiceRepository CONSUMED_INVOICE_REPOSITORY = new MySqlConsumedInvoiceRepository(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlBodyReceiptInvoice BODY_RECEIPT_INVOICE_REPOSITORY = new MySqlBodyReceiptInvoice(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlBodyConsumedInvoice BODY_CONSUMED_INVOICE_REPOSITORY = new MySqlBodyConsumedInvoice(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlPositionRepository POSITION_REPOSITORY = new MySqlPositionRepository(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final MySqlBrandRepository BRAND_REPOSITORY = new MySqlBrandRepository(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private final WarehouseView WAREHOUSE_VIEW = new WarehouseView(
			DataSourceFactory.getDataSource(DataSourceType.MY_SQL_DATASOURCE));

	private MySQLRepositoryFactory() {
	}

	public static MySQLRepositoryFactory getInstance() {
		return INSTANCE;
	}

	public MySqlClientRepository getClientRepository() {
		return CLIENT_REPOSITORY;
	}

	@Override
	public ProductRepository getProductRepository() {
		return PRODUCT_REPOSITORY;
	}

	@Override
	public EmployeeRepository getEmployeeRepository() {
		return EMPLOYEE_REPOSITORY;
	}

	@Override
	public OrderRepository getOrderRepository() {
		return ORDER_REPOSITORY;
	}

	@Override
	public CartRepository getCartRepository() {
		return CART_REPOSITORY;
	}

	@Override
	public ReceiptInvoiceRepository getReceiptInvoiceRepository() {
		return RECEIPT_INVOICE_REPOSITORY;
	}

	@Override
	public ConsumedInvoiceRepository getConsumedInvoiceRepository() {
		return CONSUMED_INVOICE_REPOSITORY;
	}

	@Override
	public BodyReceiptInvoiceRepository getBodyReceiptInvoiceRepository() {
		return BODY_RECEIPT_INVOICE_REPOSITORY;
	}

	@Override
	public BodyConsumedInvoiceRepository getBodyConsumedInvoiceRepository() {
		return BODY_CONSUMED_INVOICE_REPOSITORY;
	}

	@Override
	public PositionRepository getPositionRepository() {
		return POSITION_REPOSITORY;
	}

	@Override
	public BrandRepository getBrandRepository() {
		return BRAND_REPOSITORY;
	}

	@Override
	public View<Warehouse> getWarehouseView() {
		return WAREHOUSE_VIEW;
	}

}
