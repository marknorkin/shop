package ua.khpi.norkin.shop.repository.datasource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

public abstract class DataSourceFactory {

	private final static Logger LOG = Logger.getLogger(DataSourceFactory.class);

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			LOG.error("Cannot load MySQL Driver", e);
		}
	}

	public static DataSource getDataSource(DataSourceType type) {

		switch (type) {

		case MY_SQL_DATASOURCE:
			try {
				Context initContext = new InitialContext();

				return (DataSource) initContext
						.lookup("java:/comp/env/jdbc/shop");
			} catch (NamingException e) {
				LOG.error("Cannot get JNDI DataSource", e);
			}
		case MY_SQL_DATASOURCE_WITH_OUT_JNDI:
			try {
				Class.forName("com.mysql.jdbc.Driver");
				MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
				dataSource
						.setURL("jdbc:mysql://127.0.0.1:3306/shop?useEncoding=true&amp;characterEncoding=UTF-8");
				dataSource.setUser("root");
				dataSource.setPassword("password");
				return dataSource;
			} catch (ClassNotFoundException e) {
				LOG.error("Cannot get DataSource without JNDI", e);
			}

		default:
			throw new UnsupportedOperationException("No such DataSource: "
					+ type);
		}
	}
}
