package ua.khpi.norkin.shop.repository.datasource;

public enum DataSourceType {
	MY_SQL_DATASOURCE, MY_SQL_DATASOURCE_WITH_OUT_JNDI;
}
