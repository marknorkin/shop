package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.BodyReceiptInvoice;
import ua.khpi.norkin.shop.repository.BodyReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;

public class MySqlBodyReceiptInvoice extends DatabaseAbstractRepository<BodyReceiptInvoice> implements
		BodyReceiptInvoiceRepository {

	private static final String FIND_ALL_BODY_RECEIPT_INVOICES = "SELECT * FROM shop.body_receipt_invoice;";
	private static final String FIND_BODY_RECEIPT_INVOICE = "SELECT * FROM shop.body_receipt_invoice WHERE body_receipt_invoice.id = ? LIMIT 1;";
	private static final String FIND_BODIES_BY_INVOICE_ID = "SELECT * FROM shop.body_receipt_invoice WHERE body_receipt_invoice.receipt_invoice_id = ?;";
	private static final String INSERT_BODY_RECEIPT_INVOICE = "INSERT INTO shop.body_receipt_invoice(body_receipt_invoice.product_id, body_receipt_invoice.receipt_invoice_id, body_receipt_invoice.buy_price, body_receipt_invoice.sell_price, body_receipt_invoice.quantity) VALUES (?,?,?,?,?);";
	private static final String UPDATE_BODY_RECEIPT_INVOICE = "UPDATE shop.body_receipt_invoice SET product_id=?, receipt_invoice_id=?, buy_price=?, sell_price=?, quantity=? WHERE body_receipt_invoice.id= ? LIMIT 1;";

	private final static Logger LOG = Logger.getLogger(MySqlBodyReceiptInvoice.class);

	public MySqlBodyReceiptInvoice(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public void create(BodyReceiptInvoice entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_BODY_RECEIPT_INVOICE, PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setLong(counter++, entity.getProductId());
			pstmt.setLong(counter++, entity.getReceiptInvoiceId());
			pstmt.setBigDecimal(counter++, entity.getBuyPrice());
			pstmt.setBigDecimal(counter++, entity.getSellPrice());
			pstmt.setLong(counter++, entity.getQuantity());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a body receipt invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}

	}

	@Override
	public void update(BodyReceiptInvoice entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_BODY_RECEIPT_INVOICE);
			int counter = 1;
			pstmt.setLong(counter++, entity.getProductId());
			pstmt.setLong(counter++, entity.getReceiptInvoiceId());
			pstmt.setBigDecimal(counter++, entity.getBuyPrice());
			pstmt.setBigDecimal(counter++, entity.getSellPrice());
			pstmt.setLong(counter++, entity.getQuantity());

			pstmt.setLong(counter, entity.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a body receipt invoice", e);
		} finally {
			close(connection);
			close(pstmt);
		}
	}

	@Override
	public BodyReceiptInvoice find(long entityPK) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BodyReceiptInvoice bodyReceiptInvoice = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_BODY_RECEIPT_INVOICE);
			pstmt.setLong(1, entityPK);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				bodyReceiptInvoice = null;
			} else {
				bodyReceiptInvoice = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a body receipt invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return bodyReceiptInvoice;
	}

	@Override
	public Collection<BodyReceiptInvoice> findByReceiptInvoiceId(long receiptInvoiceId) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<BodyReceiptInvoice> bodyReceiptInvoices = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_BODIES_BY_INVOICE_ID);
			pstmt.setLong(1, receiptInvoiceId);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				bodyReceiptInvoices.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all bodies from receipt invoice by ID", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return bodyReceiptInvoices;
	}

	@Override
	public Collection<BodyReceiptInvoice> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<BodyReceiptInvoice> bodyReceiptInvoices = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_BODY_RECEIPT_INVOICES);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				bodyReceiptInvoices.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all body receipt invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return bodyReceiptInvoices;
	}

	private static BodyReceiptInvoice unmarshal(ResultSet rs) {
		BodyReceiptInvoice bodyReceiptInvoice = new BodyReceiptInvoice();
		try {
			bodyReceiptInvoice.setId(rs.getLong(Fields.ENTITY_ID));
			bodyReceiptInvoice.setProductId(rs.getLong(Fields.BODY_RECEIPT_INVOICE_PRODUCT_ID));
			bodyReceiptInvoice.setReceiptInvoiceId(rs.getLong(Fields.BODY_RECEIPT_INVOICE_RECEIPT_INVOICE_ID));
			bodyReceiptInvoice.setBuyPrice(rs.getBigDecimal(Fields.BODY_RECEIPT_INVOICE_BUY_PRICE));
			bodyReceiptInvoice.setSellPrice(rs.getBigDecimal(Fields.BODY_RECEIPT_INVOICE_SELL_PRICE));
			bodyReceiptInvoice.setQuantity(rs.getLong(Fields.BODY_RECEIPT_INVOICE_QUANTITY));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to body receipt invoice", e);
		}
		return bodyReceiptInvoice;
	}

}
