package ua.khpi.norkin.shop.repository.factory;

import ua.khpi.norkin.shop.model.dbview.Warehouse;
import ua.khpi.norkin.shop.repository.BodyConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.BodyReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.CartRepository;
import ua.khpi.norkin.shop.repository.ClientRepository;
import ua.khpi.norkin.shop.repository.ConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.EmployeeRepository;
import ua.khpi.norkin.shop.repository.OrderRepository;
import ua.khpi.norkin.shop.repository.PositionRepository;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.ReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.View;

public abstract class RepositoryFactory {

	public static RepositoryFactory getFactoryByName(FactoryType factoryType) {

		switch (factoryType) {
		case MYSQL_REPOSITORY_FACTORY:
			return MySQLRepositoryFactory.getInstance();
		default:
			throw new UnsupportedOperationException("no such factory");
		}
	}

	public abstract ClientRepository getClientRepository();

	public abstract EmployeeRepository getEmployeeRepository();

	public abstract PositionRepository getPositionRepository();

	public abstract BrandRepository getBrandRepository();

	public abstract ProductRepository getProductRepository();

	public abstract OrderRepository getOrderRepository();

	public abstract CartRepository getCartRepository();

	public abstract ReceiptInvoiceRepository getReceiptInvoiceRepository();

	public abstract ConsumedInvoiceRepository getConsumedInvoiceRepository();

	public abstract BodyReceiptInvoiceRepository getBodyReceiptInvoiceRepository();

	public abstract BodyConsumedInvoiceRepository getBodyConsumedInvoiceRepository();

	public abstract View<Warehouse> getWarehouseView();

}
