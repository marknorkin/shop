package ua.khpi.norkin.shop.repository;

import ua.khpi.norkin.shop.model.ReceiptInvoice;

public interface ReceiptInvoiceRepository extends Repository<ReceiptInvoice> {

}
