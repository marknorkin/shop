package ua.khpi.norkin.shop.repository;

public class Fields {

	public static final String ENTITY_ID = "id";
	public static final String GENERATED_KEY = "GENERATED_KEY";

	public static final String EMPLOYEE_FIRST_NAME = "first_name";
	public static final String EMPLOYEE_LAST_NAME = "last_name";
	public static final String EMPLOYEE_EMAIL = "email";
	public static final String EMPLOYEE_PASSWORD = "password";
	public static final String EMPLOYEE_ACTIVE_STATUS = "is_active";
	public static final String EMPLOYEE_POSITION_ID = "position_id";

	public static final String CLIENT_FIRST_NAME = "first_name";
	public static final String CLIENT_LAST_NAME = "last_name";
	public static final String CLIENT_EMAIL = "email";
	public static final String CLIENT_PASSWORD = "password";
	public static final String CLIENT_CITY = "city";
	public static final String CLIENT_STREET = "street";
	public static final String CLIENT_HOUSE = "house";
	public static final String CLIENT_APPARTEMENT = "appartement";
	public static final String CLIENT_PHONE = "phone";
	public static final String CLIENT_ACTIVE_STATUS = "is_active";

	public static final String PRODUCT_NAME = "name";
	public static final String PRODUCT_DESCRIPTION = "description";
	public static final String PRODUCT_IMAGE_URL = "image_url";
	public static final String PRODUCT_GENDER = "gender";
	public static final String PRODUCT_SIZE = "size";
	public static final String PRODUCT_BRAND_ID = "brand_id";

	public static final String BRAND_NAME = "name";

	public static final String POSITION_NAME = "name";

	public static final String ORDER_CLIENT_ID = "client_id";
	public static final String ORDER_DATE_ORDER = "date_order";
	public static final String ORDER_DATE_SEND = "date_send";
	public static final String ORDER_DATE_PAYMENT = "date_payment";
	public static final String ORDER_SUM = "sum";
	public static final String ORDER_EXPRESS_INVOICE_ID = "id_express_invoice";
	public static final String ORDER_CONSUMED_INVOICE_ID = "consumed_invoice_id";
	public static final String ORDER_EMPLOYEE_ID = "employee_id";

	public static final String CART_PRODUCT_ID = "product_id";
	public static final String CART_ORDER_ID = "order_id";
	public static final String CART_QUANTITY = "quantity";
	public static final String CART_RECEIPT_INVOICE_ID = "receipt_invoice_id";

	public static final String RECEIPT_INVOICE_DATE_RECEIPT = "date_receipt";
	public static final String RECEIPT_INVOICE_TYPE = "receipt_type";
	public static final String RECEIPT_INVOICE_EMPLOYEE_ID = "employee_id";

	public static final String BODY_RECEIPT_INVOICE_PRODUCT_ID = "product_id";
	public static final String BODY_RECEIPT_INVOICE_RECEIPT_INVOICE_ID = "receipt_invoice_id";
	public static final String BODY_RECEIPT_INVOICE_BUY_PRICE = "buy_price";
	public static final String BODY_RECEIPT_INVOICE_SELL_PRICE = "sell_price";
	public static final String BODY_RECEIPT_INVOICE_QUANTITY = "quantity";

	public static final String CONSUMED_INVOICE_DATE_CONSUMED = "date_consumed";
	public static final String CONSUMED_INVOICE_TYPE = "consumed_type";
	public static final String CONSUMED_INVOICE_IS_CONSUMED = "is_consumed";
	public static final String CONSUMED_INVOICE_EMPLOYEE_ID = "employee_id";

	public static final String BODY_CONSUMED_INVOICE_PRODUCT_ID = "product_id";
	public static final String BODY_CONSUMED_INVOICE_CONSUMED_INVOICE_ID = "consumed_invoice_id";
	public static final String BODY_CONSUMED_INVOICE_QUANTITY = "quantity";
	public static final String BODY_CONSUMED_INVOICE_RECEIPT_INVOICE_ID = "receipt_invoice_id";

	public static final String WAREHOUSE_RECEIPT_ID = "receipt_id";
	public static final String WAREHOUSE_PRODUCT_ID = "product_id";
	public static final String WAREHOUSE_QUANTITY_LEFT = "quantity_left";
	public static final String WAREHOUSE_SELL_PRICE = "sell_price";
	public static final String WAREHOUSE_BUY_PRICE = "buy_price";
	public static final String WAREHOUSE_DATE_RECEIPT = "date_receipt";
}
