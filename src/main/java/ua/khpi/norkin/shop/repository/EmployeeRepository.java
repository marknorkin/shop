package ua.khpi.norkin.shop.repository;

import ua.khpi.norkin.shop.model.Employee;

public interface EmployeeRepository extends Repository<Employee> {

	/**
	 * Finds Employee in database by specified email, it can be done because email
	 * should be unique.
	 *
	 * @param email
	 *            - Employee email
	 * @return Employee instance with such email
	 */
	public Employee find(String email);

	/**
	 * Finds Employee in database by specified login and password. Mainly used
	 * to login Employee in system.
	 *
	 * @param email
	 *            - Employee email
	 * @param password
	 *            - Employee password
	 * @return Employee with such fields
	 */
	public Employee find(String email, String password);
}
