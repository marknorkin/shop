package ua.khpi.norkin.shop.repository;

import ua.khpi.norkin.shop.model.Client;

public interface ClientRepository extends Repository<Client> {

	/**
	 * Finds Client in database by specified email, it can be done because email
	 * should be unique.
	 *
	 * @param email
	 *            - Client email
	 * @return Client instance with such email
	 */
	public Client find(String email);

	/**
	 * Finds Client in database by specified login and password. Mainly used to
	 * login Client in system.
	 *
	 * @param email
	 *            - Client email
	 * @param password
	 *            - Client password
	 * @return Client with such fields
	 */
	public Client find(String email, String password);
}
