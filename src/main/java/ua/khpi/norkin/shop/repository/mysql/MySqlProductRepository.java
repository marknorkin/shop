package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Gender;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.model.Size;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.ProductRepository;

public class MySqlProductRepository extends DatabaseAbstractRepository<Product> implements ProductRepository {
	private final static Logger LOG = Logger.getLogger(MySqlProductRepository.class);
	private static final String INSERT_PRODUCT = "INSERT INTO shop.product(product.name, product.description, product.image_url,product.gender,product.size,product.brand_id) VALUES(?,?,?,?,?,?);";
	private static final String UPDATE_PRODUCT = "UPDATE product SET product.name=?, product.description=?, product.image_url=?,product.gender=?,product.size=?,product.brand_id=? WHERE product.id=? LIMIT 1;";
	private static final String FIND_ALL_PRODUCTS = "SELECT * FROM shop.product;";
	private static final String FIND_PRODUCT = "SELECT * FROM shop.product WHERE product.id = ? LIMIT 1;";
	private static final String FIND_PRODUCT_BY_NAME = "SELECT * FROM shop.product WHERE product.name = ? LIMIT 1;";
	private static final String RECORDS_COUNT = "SELECT COUNT(id) AS id FROM shop.product;";
	private static final String FIND_PRODUCTS_PAGE = "SELECT * FROM shop.product WHERE id >=? LIMIT ?;";

	public MySqlProductRepository(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public void create(Product entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {

			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_PRODUCT, PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setString(counter++, entity.getName());
			pstmt.setString(counter++, entity.getDescription());
			pstmt.setString(counter++, entity.getImageUrl());
			pstmt.setString(counter++, entity.getGender().getValueEng());
			pstmt.setString(counter++, entity.getSize().name().toLowerCase());
			pstmt.setLong(counter++, entity.getBrandId());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a product", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}
	}

	@Override
	public void update(Product entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_PRODUCT);
			int counter = 1;
			pstmt.setString(counter++, entity.getName());
			pstmt.setString(counter++, entity.getDescription());
			pstmt.setString(counter++, entity.getImageUrl());
			pstmt.setString(counter++, entity.getGender().getValueEng());
			pstmt.setString(counter++, entity.getSize().name().toLowerCase());
			pstmt.setLong(counter++, entity.getBrandId());

			pstmt.setLong(counter, entity.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a product", e);
		} finally {
			close(connection);
			close(pstmt);
		}
	}

	@Override
	public Product find(long entityPK) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Product product = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_PRODUCT);
			pstmt.setLong(1, entityPK);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				product = null;
			} else {
				product = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a product", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return product;
	}

	@Override
	public Product find(String name) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Product product = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_PRODUCT_BY_NAME);
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				product = null;
			} else {
				product = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a product by name", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return product;
	}

	@Override
	public Collection<Product> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Product> products = new ArrayList<Product>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_PRODUCTS);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				products.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all products", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return products;
	}

	@Override
	public long recordsCount() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		long count = 0;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(RECORDS_COUNT);

			rs = pstmt.executeQuery();
			connection.commit();
			rs.next();
			count = rs.getLong(Fields.ENTITY_ID);
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not get products count", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return count;
	}

	@Override
	public Collection<Product> findProducts(long from, int howMany) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Product> products = new ArrayList<Product>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_PRODUCTS_PAGE);
			pstmt.setLong(1, from);
			pstmt.setInt(2, howMany);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				products.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find products for page: " + from, e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return products;
	}

	private static Product unmarshal(ResultSet rs) {

		Product product = new Product();
		try {
			product.setId(rs.getLong(Fields.ENTITY_ID));
			product.setName(rs.getString(Fields.PRODUCT_NAME));
			product.setDescription(rs.getString(Fields.PRODUCT_DESCRIPTION));
			product.setImageUrl(rs.getString(Fields.PRODUCT_IMAGE_URL));
			product.setGender(Gender.getGenderByValue(rs.getString(Fields.PRODUCT_GENDER)));
			product.setSize(Size.valueOf(rs.getString(Fields.PRODUCT_SIZE).toUpperCase()));
			product.setBrandId(rs.getLong(Fields.PRODUCT_BRAND_ID));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to product", e);
		}
		return product;
	}
}
