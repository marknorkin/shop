package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Position;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.PositionRepository;

public class MySqlPositionRepository extends DatabaseAbstractRepository<Position>
		implements PositionRepository {

	private static final String FIND_ALL_POSITIONS = "SELECT * FROM shop.position;";
	private static final String FIND_POSITION = "SELECT * FROM shop.position WHERE position.id = ? LIMIT 1;";
	private static final String INSERT_POSITION = "INSERT INTO shop.position(position.name) VALUES (?);";
	private static final String UPDATE_POSITION = "UPDATE shop.position SET name=? WHERE position.id= ? LIMIT 1;";

	private final static Logger LOG = Logger
			.getLogger(MySqlPositionRepository.class);
	public MySqlPositionRepository(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public void create(Position entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {

			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_POSITION,
					PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setString(counter++, entity.getName());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a position", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}
	}

	@Override
	public void update(Position entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_POSITION);
			int counter = 1;
			pstmt.setString(counter++, entity.getName());

			pstmt.setLong(counter, entity.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a position", e);
		} finally {
			close(connection);
			close(pstmt);
		}

	}

	@Override
	public Position find(long entityPK) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Position position = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_POSITION);
			pstmt.setLong(1, entityPK);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				position = null;
			} else {
				position = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a position", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return position;
	}

	@Override
	public Collection<Position> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Position> positions = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_POSITIONS);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				positions.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all positions", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return positions;
	}

	private static Position unmarshal(ResultSet rs) {
		Position position = new Position();
		try {
			position.setId(rs.getLong(Fields.ENTITY_ID));
			position.setName(rs.getString(Fields.POSITION_NAME));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to position", e);
		}
		return position;
	}

}
