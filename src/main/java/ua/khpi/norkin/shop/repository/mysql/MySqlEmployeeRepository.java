package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Employee;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.EmployeeRepository;
import ua.khpi.norkin.shop.repository.Fields;

public class MySqlEmployeeRepository extends
		DatabaseAbstractRepository<Employee> implements EmployeeRepository {

	private static final String FIND_ALL_EMPLOYEES = "SELECT * FROM shop.employee;";
	private static final String FIND_EMPLOYEE = "SELECT * FROM shop.employee WHERE employee.id = ? LIMIT 1;";
	private static final String FIND_EMPLOYEE_BY_EMAIL_AND_PASS = "SELECT * FROM shop.employee WHERE employee.email = ? AND employee.password=? LIMIT 1;";
	private static final String FIND_EMPLOYEE_BY_EMAIL = "SELECT * FROM shop.employee WHERE employee.email = ? LIMIT 1;";
	private static final String INSERT_EMPLOYEE = "INSERT INTO shop.employee(employee.first_name,employee.last_name,employee.email,employee.password,employee.is_active) VALUES (?,?,?,?,?);";
	private static final String UPDATE_EMPLOYEE = "UPDATE shop.employee SET first_name=?,last_name=?,email=?,password=?, is_active=? WHERE employee.id= ? LIMIT 1;";

	private final static Logger LOG = Logger
			.getLogger(MySqlEmployeeRepository.class);

	public MySqlEmployeeRepository(DataSource dataSource) {
		super(dataSource);
	}

	public void create(Employee employee) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {

			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_EMPLOYEE,
					PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setString(counter++, employee.getFirstName());
			pstmt.setString(counter++, employee.getLastName());
			pstmt.setString(counter++, employee.getEmail());
			pstmt.setString(counter++, employee.getPassword());
			pstmt.setBoolean(counter, employee.getActiveStatus());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				employee.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a employee", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}
	}

	public void update(Employee employee) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_EMPLOYEE);
			int counter = 1;
			pstmt.setString(counter++, employee.getFirstName());
			pstmt.setString(counter++, employee.getLastName());
			pstmt.setString(counter++, employee.getEmail());
			pstmt.setString(counter++, employee.getPassword());
			pstmt.setBoolean(counter++, employee.getActiveStatus());

			pstmt.setLong(counter, employee.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a employee", e);
		} finally {
			close(connection);
			close(pstmt);
		}
	}

	public Employee find(long employeeId) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Employee employee = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_EMPLOYEE);
			pstmt.setLong(1, employeeId);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				employee = null;
			} else {
				employee = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a employee", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return employee;
	}

	@Override
	public Employee find(String email, String password) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Employee employee = null;
		try {
			connection = getConnection();
			pstmt = connection
					.prepareStatement(FIND_EMPLOYEE_BY_EMAIL_AND_PASS);
			pstmt.setString(1, email);
			pstmt.setString(2, password);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				employee = null;
			} else {
				employee = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a employee", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return employee;
	}

	@Override
	public Employee find(String email) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Employee employee = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_EMPLOYEE_BY_EMAIL);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				employee = null;
			} else {
				employee = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a employee", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return employee;
	}

	public List<Employee> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Employee> employees = new ArrayList<Employee>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_EMPLOYEES);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				employees.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all employees", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return employees;
	}

	/**
	 * Unmarshals Employee record in database to Java instance.
	 *
	 * @param rs
	 *            - record from result set
	 * @return Employee instance of database record.
	 */
	private static Employee unmarshal(ResultSet rs) {
		Employee employee = new Employee();
		try {
			employee.setId(rs.getLong(Fields.ENTITY_ID));
			employee.setFirstName(rs.getString(Fields.EMPLOYEE_FIRST_NAME));
			employee.setLastName(rs.getString(Fields.EMPLOYEE_LAST_NAME));
			employee.setEmail(rs.getString(Fields.EMPLOYEE_EMAIL));
			employee.setPassword(rs.getString(Fields.EMPLOYEE_PASSWORD));
			employee.setActiveStatus(rs
					.getBoolean(Fields.EMPLOYEE_ACTIVE_STATUS));
			employee.setPositionId(rs.getLong(Fields.EMPLOYEE_POSITION_ID));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to employee", e);
		}
		return employee;
	}

}
