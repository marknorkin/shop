package ua.khpi.norkin.shop.repository;

import java.util.Collection;

import ua.khpi.norkin.shop.model.Cart;

public interface CartRepository extends Repository<Cart> {

	public Collection<Cart> findByOrder(long orderId);
}
