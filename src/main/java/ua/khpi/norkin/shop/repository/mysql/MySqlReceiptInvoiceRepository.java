package ua.khpi.norkin.shop.repository.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.ReceiptInvoice;
import ua.khpi.norkin.shop.model.ReceiptType;
import ua.khpi.norkin.shop.repository.DatabaseAbstractRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.ReceiptInvoiceRepository;

public class MySqlReceiptInvoiceRepository extends
		DatabaseAbstractRepository<ReceiptInvoice> implements
		ReceiptInvoiceRepository {

	private static final String FIND_ALL_RECEIPT_INVOICES = "SELECT * FROM shop.receipt_invoice;";
	private static final String FIND_RECEIPT_INVOICE = "SELECT * FROM shop.receipt_invoice WHERE receipt_invoice.id = ? LIMIT 1;";
	private static final String INSERT_RECEIPT_INVOICE = "INSERT INTO shop.receipt_invoice(receipt_invoice.date_receipt,receipt_invoice.receipt_type,receipt_invoice.employee_id) VALUES (?,?,?);";
	private static final String UPDATE_RECEIPT_INVOICE = "UPDATE shop.receipt_invoice SET date_receipt=?,receipt_type=?,employee_id=? WHERE receipt_invoice.id= ? LIMIT 1;";

	private final static Logger LOG = Logger
			.getLogger(MySqlReceiptInvoiceRepository.class);

	public MySqlReceiptInvoiceRepository(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public void create(ReceiptInvoice entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(INSERT_RECEIPT_INVOICE,
					PreparedStatement.RETURN_GENERATED_KEYS);
			int counter = 1;
			pstmt.setLong(counter++, entity.getDateReceipt());
			pstmt.setString(counter++, entity.getReceiptType().getValueEng());
			pstmt.setLong(counter++, entity.getEmployeeId());

			pstmt.execute();
			connection.commit();
			generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				entity.setId(generatedKeys.getLong(Fields.GENERATED_KEY));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not create a receipt invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(generatedKeys);
		}
	}

	@Override
	public void update(ReceiptInvoice entity) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(UPDATE_RECEIPT_INVOICE);
			int counter = 1;
			pstmt.setLong(counter++, entity.getDateReceipt());
			pstmt.setString(counter++, entity.getReceiptType().getValueEng());
			pstmt.setLong(counter++, entity.getEmployeeId());

			pstmt.setLong(counter, entity.getId());

			pstmt.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not update a receipt invoice", e);
		} finally {
			close(connection);
			close(pstmt);
		}

	}

	@Override
	public ReceiptInvoice find(long entityPK) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ReceiptInvoice receiptInvoice = null;
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_RECEIPT_INVOICE);
			pstmt.setLong(1, entityPK);
			rs = pstmt.executeQuery();
			connection.commit();
			if (!rs.next()) {
				receiptInvoice = null;
			} else {
				receiptInvoice = unmarshal(rs);
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find a receipt invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return receiptInvoice;
	}

	@Override
	public Collection<ReceiptInvoice> findAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<ReceiptInvoice> receiptInvoices = new ArrayList<>();
		try {
			connection = getConnection();
			pstmt = connection.prepareStatement(FIND_ALL_RECEIPT_INVOICES);
			rs = pstmt.executeQuery();
			connection.commit();
			while (rs.next()) {
				receiptInvoices.add(unmarshal(rs));
			}
		} catch (SQLException e) {
			rollback(connection);
			LOG.error("Can not find all receipt invoice", e);
		} finally {
			close(connection);
			close(pstmt);
			close(rs);
		}
		return receiptInvoices;
	}

	private static ReceiptInvoice unmarshal(ResultSet rs) {
		ReceiptInvoice receiptInvoice = new ReceiptInvoice();
		try {
			receiptInvoice.setId(rs.getLong(Fields.ENTITY_ID));
			receiptInvoice.setDateReceipt(rs
					.getLong(Fields.RECEIPT_INVOICE_DATE_RECEIPT));
			receiptInvoice.setReceiptType(ReceiptType.valueOf(rs.getString(
					Fields.RECEIPT_INVOICE_TYPE).toUpperCase()));
			receiptInvoice.setEmployeeId(rs
					.getLong(Fields.RECEIPT_INVOICE_EMPLOYEE_ID));
		} catch (SQLException e) {
			LOG.error("Can not unmarshal result set to receipt invoice", e);
		}
		return receiptInvoice;
	}

}
