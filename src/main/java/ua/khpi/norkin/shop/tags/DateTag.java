package ua.khpi.norkin.shop.tags;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class DateTag extends SimpleTagSupport {

	private long millis;

	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();

		if (millis <= 0) {
			out.write("N/A");
		} else {
			Date date = new Date(millis);
			String formattedDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
			out.write(formattedDate);
		}
	}

	public long getMillis() {
		return millis;
	}

	public void setMillis(long millis) {
		this.millis = millis;
	}

}
