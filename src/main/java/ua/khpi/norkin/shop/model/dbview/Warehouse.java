package ua.khpi.norkin.shop.model.dbview;

import java.io.Serializable;
import java.math.BigDecimal;

public class Warehouse implements Serializable {
	private static final long serialVersionUID = 1L;

	private long receiptId;
	private long productId;
	private long quantityLeft;
	private BigDecimal buyPrice;
	private BigDecimal sellPrice;
	private long dateReceipt;

	public Warehouse() {
	}

	public Warehouse(long receiptId, long productId, long quantityLeft,
			BigDecimal buyPrice, BigDecimal sellPrice, long dateReceipt) {
		super();
		this.receiptId = receiptId;
		this.productId = productId;
		this.quantityLeft = quantityLeft;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
		this.dateReceipt = dateReceipt;
	}

	public long getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(long receiptId) {
		this.receiptId = receiptId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getQuantityLeft() {
		return quantityLeft;
	}

	public void setQuantityLeft(long quantityLeft) {
		this.quantityLeft = quantityLeft;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public long getDateReceipt() {
		return dateReceipt;
	}

	public void setDateReceipt(long dateReceipt) {
		this.dateReceipt = dateReceipt;
	}

	@Override
	public String toString() {
		return "Warehouse [receiptId=" + receiptId + ", productId=" + productId
				+ ", quantityLeft=" + quantityLeft + ", buyPrice=" + buyPrice
				+ ", sellPrice=" + sellPrice + ", dateReceipt=" + dateReceipt
				+ "]";
	}

}
