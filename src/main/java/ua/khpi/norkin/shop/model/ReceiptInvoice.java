package ua.khpi.norkin.shop.model;

public class ReceiptInvoice extends Entity {

	private static final long serialVersionUID = 1L;
	private long dateReceipt;
	private ReceiptType receiptType;
	private long employeeId;

	public ReceiptInvoice(long date_receipt, ReceiptType receiptType,
			long employeeId) {
		super();
		this.dateReceipt = date_receipt;
		this.receiptType = receiptType;
		this.employeeId = employeeId;
	}

	public ReceiptInvoice() {
	}

	public long getDateReceipt() {
		return dateReceipt;
	}

	public void setDateReceipt(long date_receipt) {
		this.dateReceipt = date_receipt;
	}

	public ReceiptType getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(ReceiptType receiptType) {
		this.receiptType = receiptType;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	@Override
	public String toString() {
		return "ReceiptInvoice [dateReceipt=" + dateReceipt + ", receiptType="
				+ receiptType + ", employeeId=" + employeeId + "]";
	}

}
