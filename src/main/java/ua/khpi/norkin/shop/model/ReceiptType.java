package ua.khpi.norkin.shop.model;

public enum ReceiptType {
	PURCHASE("закупка", "purchase"), RETURN("возврат", "return");

	private final String valueRus;
	private final String valueEng;
	public static final String[] rusValues = new String[] { "закупка",
			"возврат" };

	ReceiptType(String valueRus, String valueEng) {
		this.valueEng = valueEng;
		this.valueRus = valueRus;
	}

	public String getValueRus() {
		return valueRus;
	}

	public String getValueEng() {
		return valueEng;
	}

	public static ReceiptType getTypeByValue(String receiptType) {
		for (ReceiptType rType : ReceiptType.values()) {
			if (rType.getValueRus().equals(receiptType)
					|| rType.getValueEng().equals(receiptType)) {
				return rType;
			}
		}
		return null;
	}

}
