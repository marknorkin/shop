package ua.khpi.norkin.shop.model;

public class Cart extends Entity {

	private static final long serialVersionUID = 1L;
	private long orderId;
	private long productId;
	private long quantity;
	private long receiptInvoiceId;

	public Cart() {
	}

	public Cart(long orderId, long productId, long quantity,
			long receiptInvoiceId) {
		super();
		this.orderId = orderId;
		this.productId = productId;
		this.quantity = quantity;
		this.receiptInvoiceId = receiptInvoiceId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public long getReceiptInvoiceId() {
		return receiptInvoiceId;
	}

	public void setReceiptInvoiceId(long receiptInvoiceId) {
		this.receiptInvoiceId = receiptInvoiceId;
	}

	@Override
	public String toString() {
		return "Cart [orderId=" + orderId + ", productId=" + productId
				+ ", quantity=" + quantity + ", receiptInvoiceId="
				+ receiptInvoiceId + "]";
	}

}
