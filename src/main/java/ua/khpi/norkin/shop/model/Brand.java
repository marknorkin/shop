package ua.khpi.norkin.shop.model;

public class Brand extends Entity {

	private static final long serialVersionUID = 1L;

	private String name;

	public Brand(String name) {
		super();
		this.name = name;
	}

	public Brand() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Brand [name=" + name + "]";
	}

}
