package ua.khpi.norkin.shop.model;

import java.math.BigDecimal;

public class BodyReceiptInvoice extends Entity {

	private static final long serialVersionUID = 1L;
	private long productId;
	private long receiptInvoiceId;
	private BigDecimal buyPrice;
	private BigDecimal sellPrice;
	private long quantity;

	public BodyReceiptInvoice() {
	}

	public BodyReceiptInvoice(long productId, long receiptInvoiceId,
			BigDecimal buyPrice, BigDecimal sellPrice, long quantity) {
		super();
		this.productId = productId;
		this.receiptInvoiceId = receiptInvoiceId;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
		this.quantity = quantity;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getReceiptInvoiceId() {
		return receiptInvoiceId;
	}

	public void setReceiptInvoiceId(long receiptInvoiceId) {
		this.receiptInvoiceId = receiptInvoiceId;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "BodyReceiptInvoice [productId=" + productId
				+ ", receiptInvoiceId=" + receiptInvoiceId + ", buyPrice="
				+ buyPrice + ", sellPrice=" + sellPrice + ", quantity="
				+ quantity + "]";
	}

}
