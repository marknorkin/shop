package ua.khpi.norkin.shop.model;

public class Product extends Entity {

	private static final long serialVersionUID = 1L;

	private String name;
	private String description;
	private String imageUrl;
	private long brandId;
	private Gender gender;
	private Size size;

	public Product(String name, String description, String imageUrl,
			long brandId, Gender gender, Size size) {
		super();
		this.name = name;
		this.description = description;
		this.imageUrl = imageUrl;
		this.brandId = brandId;
		this.gender = gender;
		this.size = size;
	}

	public Product() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public long getBrandId() {
		return brandId;
	}

	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Size getSize() {
		return size;
	}

	public void setSize(Size size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", description=" + description
				+ ", imageUrl=" + imageUrl + ", brandId=" + brandId
				+ ", gender=" + gender + ", size=" + size + "]";
	}

}
