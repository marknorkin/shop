package ua.khpi.norkin.shop.model;

public enum ConsumedType {
	SOLD("продажа", "sold"), THEFT("кража", "theft"), WASTE("порча", "waste");
//	public static String[] rusValues = new String[]{}
	private final String valueRus;
	private final String valueEng;

	ConsumedType(String valueRus, String valueEng) {
		this.valueEng = valueEng;
		this.valueRus = valueRus;
	}

	public String getValueRus() {
		return valueRus;
	}

	public String getValueEng() {
		return valueEng;
	}

}
