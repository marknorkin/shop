package ua.khpi.norkin.shop.model;

public class Position extends Entity {
	private static final long serialVersionUID = 1L;
	private String name;

	public Position(String name) {
		super();
		this.name = name;
	}

	public Position() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Position [name=" + name + "]";
	}

}
