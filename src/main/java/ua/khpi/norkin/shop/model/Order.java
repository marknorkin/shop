package ua.khpi.norkin.shop.model;

import java.math.BigDecimal;

public class Order extends Entity {

	private static final long serialVersionUID = 1L;
	private long clientId;
	private long dateOrder;
	private long dateSend;
	private long datePayment;
	private BigDecimal sum;
	private long expressInvoiceId;
	private long consumedInvoiceId;
	private long employeeId;

	public Order() {
	}

	public Order(long clientId, long dateOrder, long dateSend,
			long datePayment, BigDecimal sum, long expressInvoiceId,
			long consumedInvoiceId, long employeeId) {
		super();
		this.clientId = clientId;
		this.dateOrder = dateOrder;
		this.dateSend = dateSend;
		this.datePayment = datePayment;
		this.sum = sum;
		this.expressInvoiceId = expressInvoiceId;
		this.consumedInvoiceId = consumedInvoiceId;
		this.employeeId = employeeId;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public long getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(long dateOrder) {
		this.dateOrder = dateOrder;
	}

	public long getDateSend() {
		return dateSend;
	}

	public void setDateSend(long dateSend) {
		this.dateSend = dateSend;
	}

	public long getDatePayment() {
		return datePayment;
	}

	public void setDatePayment(long datePayment) {
		this.datePayment = datePayment;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public long getExpressInvoiceId() {
		return expressInvoiceId;
	}

	public void setExpressInvoiceId(long expressInvoiceId) {
		this.expressInvoiceId = expressInvoiceId;
	}

	public long getConsumedInvoiceId() {
		return consumedInvoiceId;
	}

	public void setConsumedInvoiceId(long consumedInvoiceId) {
		this.consumedInvoiceId = consumedInvoiceId;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	@Override
	public String toString() {
		return "Order [clientId=" + clientId + ", dateOrder=" + dateOrder
				+ ", dateSend=" + dateSend + ", datePayment=" + datePayment
				+ ", sum=" + sum + ", expressInvoiceId=" + expressInvoiceId
				+ ", consumedInvoiceId=" + consumedInvoiceId + ", employeeId="
				+ employeeId + "]";
	}

}
