package ua.khpi.norkin.shop.model;

public class ConsumedInvoice extends Entity {

	private static final long serialVersionUID = 1L;
	private long dateConsumed;
	private ConsumedType consumedType;
	private boolean isConsumed;
	private long employeeId;

	public ConsumedInvoice() {
	}

	public ConsumedInvoice(long dateConsumed, ConsumedType consumedType,
			boolean isConsumed, long employeeId) {
		super();
		this.dateConsumed = dateConsumed;
		this.consumedType = consumedType;
		this.isConsumed = isConsumed;
		this.employeeId = employeeId;
	}

	public long getDateConsumed() {
		return dateConsumed;
	}

	public void setDateConsumed(long dateConsumed) {
		this.dateConsumed = dateConsumed;
	}

	public ConsumedType getConsumedType() {
		return consumedType;
	}

	public void setConsumedType(ConsumedType consumedType) {
		this.consumedType = consumedType;
	}

	public boolean isConsumed() {
		return isConsumed;
	}

	public void setConsumed(boolean isConsumed) {
		this.isConsumed = isConsumed;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	@Override
	public String toString() {
		return "ConsumedInvoice [dateConsumed=" + dateConsumed
				+ ", consumedType=" + consumedType + ", isConsumed="
				+ isConsumed + ", employeeId=" + employeeId + "]";
	}

}
