package ua.khpi.norkin.shop.model;

public class BodyConsumedInvoice extends Entity {

	private static final long serialVersionUID = 1L;
	private long productId;
	private long consumedInvoiceId;
	private long receiptInvoiceId;
	private long quantity;

	public BodyConsumedInvoice() {
	}

	public BodyConsumedInvoice(long productId, long consumedInvoiceId,
			long receiptInvoiceId, long quantity) {
		super();
		this.productId = productId;
		this.consumedInvoiceId = consumedInvoiceId;
		this.receiptInvoiceId = receiptInvoiceId;
		this.quantity = quantity;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getConsumedInvoiceId() {
		return consumedInvoiceId;
	}

	public void setConsumedInvoiceId(long consumedInvoiceId) {
		this.consumedInvoiceId = consumedInvoiceId;
	}

	public long getReceiptInvoiceId() {
		return receiptInvoiceId;
	}

	public void setReceiptInvoiceId(long receiptInvoiceId) {
		this.receiptInvoiceId = receiptInvoiceId;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "BodyConsumedInvoice [productId=" + productId
				+ ", consumedInvoiceId=" + consumedInvoiceId
				+ ", receiptInvoiceId=" + receiptInvoiceId + ", quantity="
				+ quantity + "]";
	}

}
