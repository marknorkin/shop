package ua.khpi.norkin.shop.model;



public enum Gender {
	MALE("Мужская", "male"), FEMALE("Женская", "female");
	private final String valueRus;

	private final String valueEng;
	public static final String[] rusValues = new String[]{"Мужская", "Женская"};
	public static final String[] engValues = new String[]{"male", "female"};
	Gender(String valueRus, String valueEng) {
		this.valueRus = valueRus;
		this.valueEng = valueEng;
	}

	public String getValueEng() {
		return valueEng;
	}

	public String getValueRus() {
		return valueRus;
	}

	public static Gender getGenderByValue(String value) {
		for (Gender gender : Gender.values()) {
			if (gender.getValueEng().equalsIgnoreCase(value)
					|| gender.getValueRus().equalsIgnoreCase(value)) {
				return gender;
			}
		}
		return null;
	}
}
