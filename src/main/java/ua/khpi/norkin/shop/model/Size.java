package ua.khpi.norkin.shop.model;

public enum Size {
	XS, S, M, L, XL, XXL;
	public static final String[] sizes = new String[]{"XS","S","M","L","XL","XXL"};
	public boolean equalesTo(String candidate){
		return this.name().equalsIgnoreCase(candidate);
	}
}
