package ua.khpi.norkin.shop.bean;

import java.math.BigDecimal;

import ua.khpi.norkin.shop.model.Product;

public class ProductBean extends Product {

	private static final long serialVersionUID = 1L;
	private BigDecimal sellPrice;
	private long receiptInvoiceId;
	private long quantityLeft;
	private BigDecimal sum;
	private boolean canBuy;

	public ProductBean() {
		super();
	}

	public ProductBean(Product product, BigDecimal sellPrice, long receiptId, long quantityLeft) {
		super(product.getName(), product.getDescription(), product.getImageUrl(), product.getBrandId(), product
				.getGender(), product.getSize());
		this.sellPrice = sellPrice;
		this.receiptInvoiceId = receiptId;
		this.quantityLeft = quantityLeft;
		this.canBuy = quantityLeft > 0;
		this.sum = sellPrice.multiply(BigDecimal.valueOf(quantityLeft));
		super.setId(product.getId());
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal buyPrice) {
		this.sellPrice = buyPrice;
	}

	public long getReceiptInvoiceId() {
		return receiptInvoiceId;
	}

	public void setReceiptInvoiceId(long receiptInvoiceId) {
		this.receiptInvoiceId = receiptInvoiceId;
	}

	public long getQuantityLeft() {
		return quantityLeft;
	}

	public void setQuantityLeft(long quantityLeft) {
		this.quantityLeft = quantityLeft;
	}

	@Override
	public String toString() {
		return "ProductBean [buyPrice=" + sellPrice + "]";
	}
}
