package ua.khpi.norkin.shop.action.order;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.bean.ProductBean;
import ua.khpi.norkin.shop.model.BodyReceiptInvoice;
import ua.khpi.norkin.shop.model.Cart;
import ua.khpi.norkin.shop.model.Employee;
import ua.khpi.norkin.shop.model.Order;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.repository.BodyReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.CartRepository;
import ua.khpi.norkin.shop.repository.EmployeeRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.OrderRepository;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class ViewOrderAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(ViewOrderAction.class);
	private static final OrderRepository orderRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getOrderRepository();
	private static final CartRepository cartRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getCartRepository();
	private static final EmployeeRepository employeeRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getEmployeeRepository();
	private static final BodyReceiptInvoiceRepository bodyReceiptInvoiceRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY).getBodyReceiptInvoiceRepository();
	private static final ProductRepository productRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getProductRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		long orderId = StringUtils.getPositiveLong(request.getParameter(Fields.ENTITY_ID));

		Order order = orderRepository.find(orderId);
		request.setAttribute("order", order);

		Collection<Cart> carts = cartRepository.findByOrder(order.getId());
		request.setAttribute("carts", carts);

		LinkedList<BodyReceiptInvoice> neededBodies = new LinkedList<BodyReceiptInvoice>();
		LinkedList<Product> orderedProducts = new LinkedList<>();
		List<ProductBean> productBeans = new ArrayList<>();
		for (Cart cart : carts) {
			List<BodyReceiptInvoice> filtered = bodyReceiptInvoiceRepository
					.findByReceiptInvoiceId(cart.getReceiptInvoiceId()).stream()
					.filter(x -> x.getProductId() == cart.getProductId()).collect(Collectors.toList());
			neededBodies.addAll(filtered);

			Product product = productRepository.find(neededBodies.getLast().getProductId());
			orderedProducts.add(product);
			productBeans.add(new ProductBean(product, neededBodies.getLast().getSellPrice(),
					cart.getReceiptInvoiceId(), cart.getQuantity()));
		}
		request.setAttribute("productBeans", productBeans);

		Employee employee = employeeRepository.find(order.getEmployeeId());
		request.setAttribute("employee", employee);
		LOG.trace("End execute");
		return SUCCESS;
	}
}
