package ua.khpi.norkin.shop.action.position;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Position;
import ua.khpi.norkin.shop.repository.PositionRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class AddPositionAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(AddPositionAction.class);

	private static final PositionRepository positionRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getPositionRepository();

	public String name;

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		Position position = new Position(name);
		positionRepository.create(position);
		LOG.debug("Position created: " + position);
		LOG.trace("Finished execute");
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(name)) {
			addFieldError("name", "Обязательное поле");
		} else {
			addActionMessage("Должность добавлен.");
		}
	}
}
