package ua.khpi.norkin.shop.action.employee;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Employee;
import ua.khpi.norkin.shop.repository.EmployeeRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.opensymphony.xwork2.ActionSupport;

public class ConfirmEmployeeRegistrationAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger
			.getLogger(ConfirmEmployeeRegistrationAction.class);

	private static final EmployeeRepository employeeRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getEmployeeRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		String encryptedEmail = ServletActionContext.getRequest().getParameter(
				"ID");

		LOG.trace("Fetch 'ID' parameter from request: " + encryptedEmail);
		String decodedEmail = new String(Base64.getDecoder().decode(
				encryptedEmail), StandardCharsets.UTF_8);

		LOG.trace("Decode 'ID' to following email: " + decodedEmail);
		Employee employee = employeeRepository.find(decodedEmail);

		if (employee.getEmail().equals(decodedEmail)) {
			LOG.debug("Employee with not active status found in database.");
			employee.setActiveStatus(true);
			employeeRepository.update(employee);
			LOG.debug("Employee active status updated");
			LOG.trace("Finished execute");
			return SUCCESS;
		} else {
			LOG.error("Employee not found with such email: " + decodedEmail);
			LOG.trace("Finished execute");
			return ERROR;
		}
	}
}
