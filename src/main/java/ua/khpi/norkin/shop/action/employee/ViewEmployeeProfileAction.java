package ua.khpi.norkin.shop.action.employee;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Employee;

import com.opensymphony.xwork2.ActionSupport;

public class ViewEmployeeProfileAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	public Employee employee;

	public String execute() throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		employee = (Employee) session.getAttribute("employee");

		return SUCCESS;
	}

}
