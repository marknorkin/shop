package ua.khpi.norkin.shop.action.image;

import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.utils.ProductUtils;

import com.opensymphony.xwork2.ActionSupport;

public class LoadImageToViewAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(LoadImageToViewAction.class);
	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		String imageName = ServletActionContext.getRequest().getParameter("name");
		byte[] image = ProductUtils.getProductImage(imageName);
		OutputStream os = ServletActionContext.getResponse().getOutputStream();
		os.write(image);
		os.flush();
		LOG.trace("End execute");
		return NONE;
	}

}
