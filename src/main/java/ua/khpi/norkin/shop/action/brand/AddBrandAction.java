package ua.khpi.norkin.shop.action.brand;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Brand;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class AddBrandAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(AddBrandAction.class);

	private static final BrandRepository brandRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getBrandRepository();

	public String name;

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		Brand brand = new Brand(name);
		brandRepository.create(brand);
		LOG.debug("Brand created: " + brand);
		LOG.trace("Finished execute");
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(name)) {
			addFieldError("name", "Обязательное поле");
		} else {
			addActionMessage("Производитель добавлен.");
		}
	}
}
