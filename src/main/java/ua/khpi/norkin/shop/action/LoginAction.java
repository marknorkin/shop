package ua.khpi.norkin.shop.action;

import java.util.HashSet;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Client;
import ua.khpi.norkin.shop.model.Employee;
import ua.khpi.norkin.shop.model.Position;
import ua.khpi.norkin.shop.repository.ClientRepository;
import ua.khpi.norkin.shop.repository.EmployeeRepository;
import ua.khpi.norkin.shop.repository.PositionRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(LoginAction.class);
	private static final ClientRepository clientRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getClientRepository();
	private static final EmployeeRepository employeeRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getEmployeeRepository();

	private static final PositionRepository positionRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getPositionRepository();

	public String email;
	public String password;

	@Override
	public String execute() {
		boolean isEmployee = false;
		Employee employee = null;
		Client client = clientRepository.find(email, password);
		if (client == null) {
			employee = employeeRepository.find(email, password);
			isEmployee = true;
			LOG.trace("Employee found: " + employee);
		} else {
			isEmployee = false;
			LOG.trace("Client found: " + client);
		}
		if (client == null && employee == null) {
			addActionError("Cannot find user with such login/password");
			LOG.error("errorMessage: Cannot find client/employee with such login/password");
			return INPUT;
		} else if ((isEmployee && !employee.getActiveStatus()) || (!isEmployee && !client.getActiveStatus())) {
			addActionError("You are not registered!");
			LOG.error("errorMessage: Client/Employee is not registered or did not complete his registration.");
			return INPUT;
		} else {
			HttpSession session = ServletActionContext.getRequest().getSession(true);

			if (isEmployee) {
				session.setAttribute("employee", employee);
				LOG.trace("Set the session attribute 'employee' = " + employee);
				Position position = positionRepository.find(employee.getPositionId());
				session.setAttribute("position", position);
				LOG.trace("Set the session attribute 'position' = " + position);
				LOG.info("Employee: " + employee + " logged in ");
			} else {
				session.setAttribute("client", client);
				session.setAttribute("cart", new HashSet<Long>());
				LOG.trace("Set the session attribute 'client' = " + client);
				LOG.trace("Set the empty session attribute 'cart'");
				LOG.info("Client: " + client + " logged in ");
			}

			return SUCCESS;
		}
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(email)) {
			addFieldError("email", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(password)) {
			addFieldError("password", "Обязательное поле");
		}
	}

}
