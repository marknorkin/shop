package ua.khpi.norkin.shop.action.product;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Brand;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.model.dbview.Warehouse;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.View;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.repository.mysql.view.WarehouseView;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class ViewProductAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ViewProductAction.class);

	private static final ProductRepository productRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getProductRepository();

	private static final BrandRepository brandRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getBrandRepository();

	private static final View<Warehouse> warehouse = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getWarehouseView();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		long productId = Long.valueOf(request.getParameter(Fields.ENTITY_ID));
		Product product = productRepository.find(productId);
		request.setAttribute("product", product);
		Brand brand = brandRepository.find(product.getBrandId());
		request.setAttribute("brand", brand.getName());
		request.setAttribute("sellPrice", warehouse.find(product.getId()).getSellPrice());
		// byte[] image = ProductUtils.getProductImage(product.getImageUrl());
		// request.setAttribute("image",
		// Base64.getEncoder().encodeToString(image));
		LOG.trace("Finished execute");
		return SUCCESS;
	}

}
