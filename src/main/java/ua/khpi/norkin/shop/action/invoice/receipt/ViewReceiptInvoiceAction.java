package ua.khpi.norkin.shop.action.invoice.receipt;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.BodyReceiptInvoice;
import ua.khpi.norkin.shop.model.ReceiptInvoice;
import ua.khpi.norkin.shop.repository.BodyReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.ReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class ViewReceiptInvoiceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ViewReceiptInvoiceAction.class);
	private static final ReceiptInvoiceRepository receiptInvoiceRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getReceiptInvoiceRepository();
	private static final BodyReceiptInvoiceRepository bodyReceiptInvoiceRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY).getBodyReceiptInvoiceRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		long receiptInvoiceId = StringUtils.getPositiveLong(request.getParameter(Fields.ENTITY_ID));
		ReceiptInvoice receiptInvoice = receiptInvoiceRepository.find(receiptInvoiceId);
		Collection<BodyReceiptInvoice> bodyReceiptInvoices = bodyReceiptInvoiceRepository
				.findByReceiptInvoiceId(receiptInvoice.getId());
		request.setAttribute("receiptInvoice", receiptInvoice);
		request.setAttribute("bodyReceiptInvoices", bodyReceiptInvoices);
		LOG.trace("End execute");
		return SUCCESS;
	}

}