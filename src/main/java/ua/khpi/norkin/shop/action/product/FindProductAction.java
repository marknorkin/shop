package ua.khpi.norkin.shop.action.product;

import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Brand;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.ActionSupport;

public class FindProductAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(FindProductAction.class);

	private static final ProductRepository productRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getProductRepository();

	private static final BrandRepository brandRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getBrandRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();

		String productIdentifier = request.getParameter("productIdentifier");
		long productId = StringUtils.getPositiveLong(productIdentifier);

		Product product = null;
		if (productId == -1) {
			product = productRepository.find(productIdentifier);
		} else {
			product = productRepository.find(productId);
		}
		Gson gson = new Gson();

		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		if (product != null) {
			Brand brand = brandRepository.find(product.getBrandId());

			JsonElement jsonProduct = gson.toJsonTree(product, new TypeToken<Product>() {
			}.getType());

			JsonElement jsonBrand = gson.toJsonTree(brand, new TypeToken<Brand>() {
			}.getType());
			JsonArray jsonArray = new JsonArray();
			JsonObject object = new JsonObject();
			object.add("product", jsonProduct);
			object.add("brand", jsonBrand);
			jsonArray.add(jsonProduct);
			writer.print(object);
		} else {
			JsonObject object = new JsonObject();

			object.add("msg", gson.toJsonTree("Товар не найден"));
			writer.print(object);
		}
		writer.flush();
		LOG.trace("Finished execute");
		return null;
	}

}
