package ua.khpi.norkin.shop.action.product;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Brand;
import ua.khpi.norkin.shop.model.Gender;
import ua.khpi.norkin.shop.model.Size;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.opensymphony.xwork2.ActionSupport;

public class PopulateAddProductPageAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(PopulateAddProductPageAction.class);

	private static final BrandRepository brandRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getBrandRepository();
	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		request.setAttribute("sizes", Size.sizes);
		LOG.debug("Add attribute sizes: "+ Size.sizes);
		request.setAttribute("genders", Gender.rusValues);
		LOG.debug("Add attribute genders: "+ Gender.rusValues);
		Collection<Brand> brands = brandRepository.findAll();
		request.setAttribute("brands", brands);
		LOG.debug("Add attribute brands: "+ brands);

		LOG.trace("Finished execute");
		return SUCCESS;
	}

}
