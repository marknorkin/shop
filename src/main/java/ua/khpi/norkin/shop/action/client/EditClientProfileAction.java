package ua.khpi.norkin.shop.action.client;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Client;
import ua.khpi.norkin.shop.repository.ClientRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class EditClientProfileAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(EditClientProfileAction.class);

	private static final ClientRepository clientRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getClientRepository();

	public String password;
	public String firstName;
	public String lastName;
	public String city;
	public String street;
	public String house;
	public String appartement;
	public String phone;

	@Override
	public String execute() throws Exception {
		LOG.debug("Start execute");

		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		Client client = (Client) session.getAttribute("client");
		client.setFirstName(firstName);
		client.setLastName(lastName);
		client.setPassword(password);
		client.setCity(city);
		client.setStreet(street);
		client.setHouse(house);
		client.setAppartement(appartement);
		client.setPhone(phone);
		LOG.trace("Client session record updated: " + client);
		clientRepository.update(client);
		LOG.trace("Client database record updated: " + client);

		LOG.debug("Finished execute");
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(password)) {
			addFieldError("password", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(firstName)) {
			addFieldError("firstName", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(lastName)) {
			addFieldError("lastName", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(city)) {
			addFieldError("city", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(street)) {
			addFieldError("street", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(house)) {
			addFieldError("house", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(appartement)) {
			addFieldError("appartement", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(phone)) {
			addFieldError("phone", "Обязательное поле");
		} else {
			addActionMessage("Profile successfully updated.");
		}
	}
}
