package ua.khpi.norkin.shop.action.product;

import java.io.File;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Gender;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.model.Size;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.ProductUtils;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class AddProductAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(AddProductAction.class);
	private static final ProductRepository productRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getProductRepository();
	public long id;
	public String name;
	public String description;
	public long brandId;
	public String genderValue;
	public String sizeValue;
	public File image;

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		Gender gender = Gender.getGenderByValue(genderValue);
		Size size = Size.valueOf(sizeValue);
		Product product = new Product();
		product.setName(name);
		product.setDescription(description);
		product.setBrandId(brandId);
		product.setGender(gender);
		product.setSize(size);
		String imageUrl = ProductUtils.saveProductImage(product, image);
		product.setImageUrl(imageUrl);

		productRepository.create(product);
		id = product.getId();
		LOG.debug("Product created: " + product);

		LOG.trace("Finished execute");
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(name)) {
			addFieldError("name", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(description)) {
			addFieldError("description", "Обязательное поле");
		} else if (!image.exists()) {
			addFieldError("image", "Выберите изображение");
		} else {
			addActionMessage("Товар добавлен");
		}
	}

}
