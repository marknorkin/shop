package ua.khpi.norkin.shop.action.position;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Position;
import ua.khpi.norkin.shop.repository.PositionRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class EditPositionAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(EditPositionAction.class);

	private static final PositionRepository positionRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getPositionRepository();

	public String name;
	public Position position;
	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		position.setName(name);
		positionRepository.update(position);
		LOG.debug("Position updated: " + position);
		LOG.trace("Finished execute");
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(name)) {
			addFieldError("name", "Обязательное поле");
		} else {
			addActionMessage("Должность изменена.");
		}
	}
}
