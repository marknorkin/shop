package ua.khpi.norkin.shop.action.invoice.consumed;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.ConsumedInvoice;
import ua.khpi.norkin.shop.repository.ConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.opensymphony.xwork2.ActionSupport;

public class ViewAllConsumedInvoicesAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ViewAllConsumedInvoicesAction.class);
	private static final ConsumedInvoiceRepository consumedInvoiceRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getConsumedInvoiceRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();

		Collection<ConsumedInvoice> consumedInvoices = consumedInvoiceRepository.findAll();
		request.setAttribute("consumedInvoices", consumedInvoices);
		LOG.trace("End execute");
		return SUCCESS;
	}

}