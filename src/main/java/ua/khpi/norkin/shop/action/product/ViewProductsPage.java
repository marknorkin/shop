package ua.khpi.norkin.shop.action.product;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.bean.ProductBean;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.model.dbview.Warehouse;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.View;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.ActionSupport;

public class ViewProductsPage extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ViewProductsPage.class);

	private static final ProductRepository productRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getProductRepository();
	private static final View<Warehouse> warehouseView = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getWarehouseView();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		int pageSize = 5;
		String page = request.getParameter("page");
		long from = (Long.valueOf(page) - 1) * pageSize;
		Collection<Product> products = productRepository.findProducts(from, pageSize);
		List<ProductBean> productBeans = new ArrayList<ProductBean>();
		for (Product product : products) {
			Warehouse pware = warehouseView.find(product.getId());
			productBeans.add(new ProductBean(product, pware.getSellPrice(), pware.getReceiptId(), pware
					.getQuantityLeft()));
		}

		Gson gson = new Gson();
		JsonElement element = gson.toJsonTree(productBeans, new TypeToken<List<ProductBean>>() {
		}.getType());
		JsonArray jsonArray = element.getAsJsonArray();
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.print(jsonArray);
		writer.flush();
		LOG.trace("Finished execute");
		return null;
	}
}
