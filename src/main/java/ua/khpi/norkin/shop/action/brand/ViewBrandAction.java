package ua.khpi.norkin.shop.action.brand;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Brand;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class ViewBrandAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ViewBrandAction.class);

	private static final BrandRepository brandRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getBrandRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		String id = request.getParameter(Fields.ENTITY_ID);
		long brandid = StringUtils.getPositiveLong(id);

		if (brandid == -1) {
			LOG.trace("Finished execute");
			return ERROR;
		} else {
			Brand brand = brandRepository.find(brandid);
			request.setAttribute("brand", brand);
			LOG.trace("Finished execute");
			return SUCCESS;
		}
	}

}
