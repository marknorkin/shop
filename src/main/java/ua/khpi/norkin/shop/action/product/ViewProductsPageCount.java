package ua.khpi.norkin.shop.action.product;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.opensymphony.xwork2.ActionSupport;

public class ViewProductsPageCount extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(ViewProductsPageCount.class);

	private static final ProductRepository productRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getProductRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		long recordsCount = productRepository.recordsCount();
		ServletActionContext.getRequest().setAttribute("records", recordsCount);
		LOG.trace("Finished execute");
		return SUCCESS;
	}
}
