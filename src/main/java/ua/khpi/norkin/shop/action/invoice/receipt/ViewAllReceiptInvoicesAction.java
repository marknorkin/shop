package ua.khpi.norkin.shop.action.invoice.receipt;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.ReceiptInvoice;
import ua.khpi.norkin.shop.model.ReceiptType;
import ua.khpi.norkin.shop.repository.ReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.opensymphony.xwork2.ActionSupport;

public class ViewAllReceiptInvoicesAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ViewAllReceiptInvoicesAction.class);
	private static final ReceiptInvoiceRepository receiptInvoiceRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getReceiptInvoiceRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();

		Collection<ReceiptInvoice> receiptInvoices = receiptInvoiceRepository.findAll();
		request.setAttribute("receiptInvoices", receiptInvoices);
		LOG.trace("End execute");
		return SUCCESS;
	}

}