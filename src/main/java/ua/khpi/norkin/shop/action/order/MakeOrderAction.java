package ua.khpi.norkin.shop.action.order;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javassist.expr.NewArray;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.bean.ProductBean;
import ua.khpi.norkin.shop.model.BodyConsumedInvoice;
import ua.khpi.norkin.shop.model.Cart;
import ua.khpi.norkin.shop.model.Client;
import ua.khpi.norkin.shop.model.ConsumedInvoice;
import ua.khpi.norkin.shop.model.ConsumedType;
import ua.khpi.norkin.shop.model.Order;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.repository.BodyConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.CartRepository;
import ua.khpi.norkin.shop.repository.ConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.OrderRepository;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opensymphony.xwork2.ActionSupport;

public class MakeOrderAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(MakeOrderAction.class);
	private static final CartRepository cartRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getCartRepository();
	private static final OrderRepository orderRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getOrderRepository();
	private static final BodyConsumedInvoiceRepository bodyConsumedInvoiceRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY).getBodyConsumedInvoiceRepository();
	private static final ConsumedInvoiceRepository consumedInvoiceRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getConsumedInvoiceRepository();
	private static final ProductRepository productRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getProductRepository();
	public String cartInJson;

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpSession session = ServletActionContext.getRequest().getSession(false);
		List<ProductBean> orderedProducts = convertJsonToModel(cartInJson);
		ConsumedInvoice consumedInvoice = new ConsumedInvoice(System.currentTimeMillis(), ConsumedType.SOLD, false, 0);
		consumedInvoiceRepository.create(consumedInvoice);
		BigDecimal sum = BigDecimal.ZERO;
		for (ProductBean productBean : orderedProducts) {
			BodyConsumedInvoice bodyConsumedInvoice = new BodyConsumedInvoice(productBean.getId(),
					consumedInvoice.getId(), productBean.getReceiptInvoiceId(), productBean.getQuantityLeft());

			bodyConsumedInvoiceRepository.create(bodyConsumedInvoice);

			sum = sum.add(productBean.getSellPrice().multiply(BigDecimal.valueOf(productBean.getQuantityLeft())));
		}
		Client client = (Client) session.getAttribute("client");

		Order order = new Order(client.getId(), consumedInvoice.getDateConsumed(), -1, -1, sum, -1,
				consumedInvoice.getId(), consumedInvoice.getEmployeeId());
		orderRepository.create(order);

		for (ProductBean productBean : orderedProducts) {
			Cart cart = new Cart(order.getId(), productBean.getId(), productBean.getQuantityLeft(),
					productBean.getReceiptInvoiceId());
			cartRepository.create(cart);
		}
		session.setAttribute("cart", new HashSet<Long>());
		LOG.trace("End execute");
		return SUCCESS;
	}

	private static List<ProductBean> convertJsonToModel(String value) {
		JsonParser jsonParser = new JsonParser();
		JsonArray jsonProductsInCart = (JsonArray) jsonParser.parse(value);
		List<ProductBean> productBeans = new ArrayList<>();
		for (JsonElement elem : jsonProductsInCart) {
			JsonObject jsonProduct = elem.getAsJsonObject();
			long productId = jsonProduct.get("id").getAsLong();
			long receiptInvoiceId = jsonProduct.get("receiptInvoiceId").getAsLong();
			BigDecimal sellPrice = jsonProduct.get("sellPrice").getAsBigDecimal();
			long quantityOrdered = jsonProduct.get("quantity").getAsLong();
			Product product = getProduct(productId);
			ProductBean bean = new ProductBean(product, sellPrice, receiptInvoiceId, quantityOrdered);
			productBeans.add(bean);
		}
		return productBeans;
	}

	private static final Product getProduct(long id) {
		return productRepository.find(id);
	}
}
