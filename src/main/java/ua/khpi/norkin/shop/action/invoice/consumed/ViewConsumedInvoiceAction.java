package ua.khpi.norkin.shop.action.invoice.consumed;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.BodyConsumedInvoice;
import ua.khpi.norkin.shop.model.ConsumedInvoice;
import ua.khpi.norkin.shop.repository.BodyConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.ConsumedInvoiceRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class ViewConsumedInvoiceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ViewConsumedInvoiceAction.class);
	private static final ConsumedInvoiceRepository consumedInvoiceRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getConsumedInvoiceRepository();
	private static final BodyConsumedInvoiceRepository bodyConsumedInvoiceRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY).getBodyConsumedInvoiceRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		long consumedInvoiceId = StringUtils.getPositiveLong(request.getParameter(Fields.ENTITY_ID));
		ConsumedInvoice consumedInvoice = consumedInvoiceRepository.find(consumedInvoiceId);
		Collection<BodyConsumedInvoice> bodyConsumedInvoices = bodyConsumedInvoiceRepository
				.findByInvoiceId(consumedInvoice.getId());

		request.setAttribute("consumedInvoice", consumedInvoice);
		request.setAttribute("bodyConsumedInvoices", bodyConsumedInvoices);
		LOG.trace("End execute");
		return SUCCESS;
	}

}