package ua.khpi.norkin.shop.action.invoice.consumed;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.ConsumedType;
import ua.khpi.norkin.shop.model.ReceiptType;

import com.opensymphony.xwork2.ActionSupport;

public class PopulateAddConsumedInvoiceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(PopulateAddConsumedInvoiceAction.class);

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		request.setAttribute("consumedTypes", ConsumedType.values());
		LOG.trace("End execute");
		return SUCCESS;
	}

}