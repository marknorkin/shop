package ua.khpi.norkin.shop.action.position;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Position;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.PositionRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class ViewPositionAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(ViewPositionAction.class);

	private static final PositionRepository positionRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getPositionRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		String id = request.getParameter(Fields.ENTITY_ID);
		long positionid = StringUtils.getPositiveLong(id);

		if (positionid == -1) {
			LOG.trace("Finished execute");
			return ERROR;
		} else {
			Position position = positionRepository.find(positionid);
			request.setAttribute("position", position);
			LOG.trace("Finished execute");
			return SUCCESS;
		}
	}

}
