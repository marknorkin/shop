package ua.khpi.norkin.shop.action.client;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Client;

import com.opensymphony.xwork2.ActionSupport;

public class ViewClientProfileAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	public Client client;

	public String execute() throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		client = (Client) session.getAttribute("client");
		return SUCCESS;
	}

}
