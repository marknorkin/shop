package ua.khpi.norkin.shop.action.client;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Client;
import ua.khpi.norkin.shop.repository.ClientRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.opensymphony.xwork2.ActionSupport;

public class ConfirmClientRegistrationAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger
			.getLogger(ConfirmClientRegistrationAction.class);

	private static final ClientRepository clientRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getClientRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		String encryptedEmail = ServletActionContext.getRequest().getParameter(
				"ID");

		LOG.trace("Fetch 'ID' parameter from request: " + encryptedEmail);
		String decodedEmail = new String(Base64.getDecoder().decode(
				encryptedEmail), StandardCharsets.UTF_8);

		LOG.trace("Decode 'ID' to following email: " + decodedEmail);
		Client client = clientRepository.find(decodedEmail);

		if (client.getEmail().equals(decodedEmail)) {
			LOG.debug("Client with not active status found in database.");
			client.setActiveStatus(true);
			clientRepository.update(client);
			LOG.debug("Client active status updated");
			LOG.trace("Finished execute");
			return SUCCESS;
		} else {
			LOG.error("Client not found with such email: " + decodedEmail);
			LOG.trace("Finished execute");
			return ERROR;
		}
	}
}
