package ua.khpi.norkin.shop.action.cart;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.bean.ProductBean;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.model.dbview.Warehouse;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.View;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.ActionSupport;

public class ViewCartAction extends ActionSupport {

	private static final long serialVersionUID = 2046350703022168778L;
	private final static Logger LOG = Logger.getLogger(ViewCartAction.class);

	private static final ProductRepository productRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getProductRepository();
	private static final View<Warehouse> warehouseView = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getWarehouseView();

	@SuppressWarnings("unchecked")
	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		Set<Long> orderedProducts = (Set<Long>) request.getSession(false).getAttribute("cart");
		List<ProductBean> products = new ArrayList<>();
		for (Long productId : orderedProducts) {
			Product product = productRepository.find(productId);
			LOG.debug("Find product:" + product);
			Warehouse stock = warehouseView.find(productId);
			LOG.debug("Find product on stock:" + stock);
			ProductBean productBean = new ProductBean(product, stock.getSellPrice(), stock.getReceiptId(),
					stock.getQuantityLeft());
			LOG.debug("Add product to productBean: " + productBean);
			products.add(productBean);
		}
		// request.setAttribute("clientCart", products);
		Gson gson = new Gson();
		JsonElement element = gson.toJsonTree(products, new TypeToken<List<ProductBean>>() {
		}.getType());
		JsonArray jsonArray = element.getAsJsonArray();
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.print(jsonArray);
		writer.flush();
		LOG.trace("Finished execute");
		return null;
	}
}
