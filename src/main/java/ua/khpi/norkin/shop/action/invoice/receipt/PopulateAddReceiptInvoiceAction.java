package ua.khpi.norkin.shop.action.invoice.receipt;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.ReceiptType;

import com.opensymphony.xwork2.ActionSupport;

public class PopulateAddReceiptInvoiceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(PopulateAddReceiptInvoiceAction.class);

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		request.setAttribute("receiptTypes", ReceiptType.rusValues);
		LOG.trace("End execute");
		return SUCCESS;
	}

}