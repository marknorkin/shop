package ua.khpi.norkin.shop.action.order;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Client;
import ua.khpi.norkin.shop.model.Order;
import ua.khpi.norkin.shop.repository.OrderRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.opensymphony.xwork2.ActionSupport;

public class ViewMyOrdersAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(ViewMyOrdersAction.class);
	private static final OrderRepository orderRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getOrderRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		Client client = (Client) request.getSession(false).getAttribute("client");
		Collection<Order> myOrders = orderRepository.findMyOrders(client.getId());
		request.setAttribute("myOrders", myOrders);
		LOG.trace("End execute");
		return SUCCESS;
	}

}
