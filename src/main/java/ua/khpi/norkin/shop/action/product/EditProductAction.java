package ua.khpi.norkin.shop.action.product;

import java.io.File;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Gender;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.model.Size;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.ProductUtils;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class EditProductAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(EditProductAction.class);
	private static final ProductRepository productRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getProductRepository();
	public long id;
	public String name;
	public String description;
	public long brandId;
	public String genderValue;
	public String sizeValue;
	public File image;

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");

		long productId = StringUtils.getPositiveLong(ServletActionContext.getRequest().getParameter(Fields.ENTITY_ID));

		Product product = productRepository.find(productId);

		Gender gender = Gender.getGenderByValue(genderValue);
		Size size = Size.valueOf(sizeValue);
		product.setDescription(description);
		product.setBrandId(brandId);
		product.setGender(gender);
		product.setSize(size);

		if (product.getName().equals(name) && image != null && image.exists()) {
			// file changed, name not - > delete old, create new
			ProductUtils.deleteProductImage(product.getName());
			String imageUrl = ProductUtils.saveProductImage(product, image);
			product.setImageUrl(imageUrl);
		} else if (!product.getName().equals(name) && (image == null || !image.exists())) {
			// name changed, file not ->rename
			String imageUrl = ProductUtils.renameProductImage(product.getName(), name);
			product.setImageUrl(imageUrl);
		} else if (!product.getName().equals(name) && image != null && image.exists()) {
			// file changed, name changed - > delete old, create new
			ProductUtils.deleteProductImage(product.getName());
			product.setName(name);
			String imageUrl = ProductUtils.saveProductImage(product, image);
			product.setImageUrl(imageUrl);
		}
		product.setName(name);

		productRepository.update(product);
		id = product.getId();
		LOG.debug("Product updated: " + product);

		LOG.trace("Finished execute");
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(name)) {
			addFieldError("name", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(description)) {
			addFieldError("description", "Обязательное поле");
		} else {
			addActionMessage("Товар добавлен");
		}
	}

}
