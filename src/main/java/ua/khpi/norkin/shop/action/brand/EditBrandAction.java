package ua.khpi.norkin.shop.action.brand;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Brand;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class EditBrandAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(EditBrandAction.class);

	private static final BrandRepository brandRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getBrandRepository();

	public String name;
	public Brand brand;
	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		brand.setName(name);
		brandRepository.update(brand);
		LOG.debug("Brand updated: " + brand);
		LOG.trace("Finished execute");
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(name)) {
			addFieldError("name", "Обязательное поле");
		} else {
			addActionMessage("Производитель изменен.");
		}
	}
}
