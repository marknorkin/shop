package ua.khpi.norkin.shop.action.cart;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class AddProductToCartAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(AddProductToCartAction.class);

	@SuppressWarnings("unchecked")
	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		String productId = request.getParameter("productId");
		long id = StringUtils.getPositiveLong(productId);
		if (id > 0) {
			Set<Long> clientCart = (Set<Long>) request.getSession(false).getAttribute("cart");
			LOG.trace("Get from session client cart:" + clientCart);
			clientCart.add(id);
			request.getSession(false).setAttribute("cart", clientCart);
		}
		LOG.trace("Finished execute");
		return null;
	}

}
