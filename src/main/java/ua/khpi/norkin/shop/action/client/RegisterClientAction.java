package ua.khpi.norkin.shop.action.client;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Client;
import ua.khpi.norkin.shop.repository.ClientRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.MailUtils;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class RegisterClientAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger
			.getLogger(RegisterClientAction.class);

	private static final ClientRepository clientRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getClientRepository();

	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public String city;
	public String street;
	public String house;
	public String appartement;
	public String phone;

	@Override
	public String execute() throws Exception {
		LOG.debug("Start execute");

		Client client = new Client(firstName, lastName, email, password, false,
				city, street, house, appartement, phone);

		clientRepository.create(client);
		LOG.trace("Client record created: " + client);

		MailUtils.sendConfirmationEmail(client);
		LOG.trace("Mail for completing registration sent.");
		LOG.debug("Finished execute");
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(email)) {
			addFieldError("email", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(password)) {
			addFieldError("password", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(firstName)) {
			addFieldError("firstName", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(lastName)) {
			addFieldError("lastName", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(city)) {
			addFieldError("city", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(street)) {
			addFieldError("street", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(house)) {
			addFieldError("house", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(appartement)) {
			addFieldError("appartement", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(phone)) {
			addFieldError("phone", "Обязательное поле");
		} else {
			addActionMessage("Your account was created. Check your email and confirm your registration.");
		}
	}

}
