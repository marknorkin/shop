package ua.khpi.norkin.shop.action.invoice.receipt;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.BodyReceiptInvoice;
import ua.khpi.norkin.shop.model.Employee;
import ua.khpi.norkin.shop.model.ReceiptInvoice;
import ua.khpi.norkin.shop.model.ReceiptType;
import ua.khpi.norkin.shop.repository.BodyReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.ReceiptInvoiceRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opensymphony.xwork2.ActionSupport;

public class AddReceiptInvoiceAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(AddReceiptInvoiceAction.class);
	private static final ReceiptInvoiceRepository receiptInvoiceRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getReceiptInvoiceRepository();
	private static final BodyReceiptInvoiceRepository bodyReceiptInvoiceRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getBodyReceiptInvoiceRepository();
	public String bodyRecInv;
	public String receiptType;

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");

		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		List<BodyReceiptInvoice> bodiesModels = convertJsonToModel(bodyRecInv);

		Employee employee = (Employee) session.getAttribute("employee");
		LOG.debug("Get employee from session: " + employee);
		long employeeId = employee.getId();
		long dateReceipt = System.currentTimeMillis();
		LOG.debug("Create receipt date: " + dateReceipt);
		ReceiptType type = ReceiptType.getTypeByValue(receiptType);
		LOG.debug("Get receipt type by value: " + type);
		ReceiptInvoice receiptInvoice = new ReceiptInvoice(dateReceipt, type,
				employeeId);
		receiptInvoiceRepository.create(receiptInvoice);
		LOG.debug("Create Receipt Invoice in repository: " + receiptInvoice);
		for (BodyReceiptInvoice bodyReceiptInvoice : bodiesModels) {
			bodyReceiptInvoice.setReceiptInvoiceId(receiptInvoice.getId());
			bodyReceiptInvoiceRepository.create(bodyReceiptInvoice);
			LOG.debug("Create Body Rreceipt Invoice in repo: "
					+ bodyReceiptInvoice);
		}

		LOG.trace("End execute");
		return SUCCESS;
	}

	private static List<BodyReceiptInvoice> convertJsonToModel(String value) {
		JsonParser jsonParser = new JsonParser();
		JsonArray jsonBodies = (JsonArray) jsonParser.parse(value);
		List<BodyReceiptInvoice> bodies = new ArrayList<>();
		for (JsonElement elem : jsonBodies) {
			JsonObject body = elem.getAsJsonObject();
			long productId = body.get("productId").getAsLong();
			BigDecimal sellPrice = body.get("sellPrice").getAsBigDecimal();
			BigDecimal buyPrice = body.get("buyPrice").getAsBigDecimal();
			long quantity = body.get("quantity").getAsLong();
			bodies.add(new BodyReceiptInvoice(productId, 0, buyPrice,
					sellPrice, quantity));
		}
		return bodies;
	}
	/*
	 * @Override public void validate() {
	 *
	 * if (StringUtils.isNullOrEmpty(bodyRecInv)) { addFieldError("receiptType",
	 * "Укажите тело накладной"); } else if
	 * (StringUtils.isNullOrEmpty(receiptType) ||
	 * ReceiptType.getTypeByValue(receiptType) == null) {
	 * addFieldError("receiptType", "Укажите тип прихода"); } else {
	 * addActionMessage("Накладная оформлена"); } }
	 */
}
