package ua.khpi.norkin.shop.action.position;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Position;
import ua.khpi.norkin.shop.repository.PositionRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;

import com.opensymphony.xwork2.ActionSupport;

public class ViewAllPositionsAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(ViewAllPositionsAction.class);

	private static final PositionRepository positionRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getPositionRepository();

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		Collection<Position> positions = positionRepository.findAll();
		request.setAttribute("positions", positions);
		LOG.trace("Finished execute");
		return SUCCESS;
	}

}
