package ua.khpi.norkin.shop.action;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class LogoutAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(LogoutAction.class);

	@Override
	public String execute() throws Exception {
		LOG.debug("Start logout");

		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		if (session != null) {
			session.invalidate();
		}
		LOG.debug("Finished logout");
		return SUCCESS;
	}

}
