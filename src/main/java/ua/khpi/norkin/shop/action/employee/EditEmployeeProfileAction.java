package ua.khpi.norkin.shop.action.employee;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Employee;
import ua.khpi.norkin.shop.repository.EmployeeRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class EditEmployeeProfileAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger
			.getLogger(EditEmployeeProfileAction.class);

	private static final EmployeeRepository employeeRepository = RepositoryFactory
			.getFactoryByName(FactoryType.MYSQL_REPOSITORY_FACTORY)
			.getEmployeeRepository();

	public String password;
	public String firstName;
	public String lastName;

	@Override
	public String execute() throws Exception {
		LOG.debug("Start execute");

		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		Employee employee = (Employee) session.getAttribute("employee");
		employee.setFirstName(firstName);
		employee.setLastName(lastName);
		employee.setPassword(password);

		LOG.trace("Employee session record updated: " + employee);
		employeeRepository.update(employee);
		LOG.trace("Employee database record updated: " + employee);

		LOG.debug("Finished execute");
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (StringUtils.isNullOrEmpty(password)) {
			addFieldError("password", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(firstName)) {
			addFieldError("firstName", "Обязательное поле");
		} else if (StringUtils.isNullOrEmpty(lastName)) {
			addFieldError("lastName", "Обязательное поле");
		} else {
			addActionMessage("Profile successfully updated.");
		}
	}
}
