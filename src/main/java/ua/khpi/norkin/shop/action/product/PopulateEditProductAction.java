package ua.khpi.norkin.shop.action.product;

import java.util.Base64;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import ua.khpi.norkin.shop.model.Brand;
import ua.khpi.norkin.shop.model.Gender;
import ua.khpi.norkin.shop.model.Product;
import ua.khpi.norkin.shop.model.Size;
import ua.khpi.norkin.shop.repository.BrandRepository;
import ua.khpi.norkin.shop.repository.Fields;
import ua.khpi.norkin.shop.repository.ProductRepository;
import ua.khpi.norkin.shop.repository.factory.FactoryType;
import ua.khpi.norkin.shop.repository.factory.RepositoryFactory;
import ua.khpi.norkin.shop.utils.ProductUtils;
import ua.khpi.norkin.shop.utils.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

public class PopulateEditProductAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(PopulateEditProductAction.class);
	private static final ProductRepository productRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getProductRepository();
	private static final BrandRepository brandRepository = RepositoryFactory.getFactoryByName(
			FactoryType.MYSQL_REPOSITORY_FACTORY).getBrandRepository();
	public long id;

	@Override
	public String execute() throws Exception {
		LOG.trace("Start execute");
		HttpServletRequest request = ServletActionContext.getRequest();
		long productId = StringUtils.getPositiveLong(request.getParameter(Fields.ENTITY_ID));
		if (productId > 0) {
			id = productId;
			Product product = productRepository.find(id);
			request.setAttribute("product", product);
			LOG.debug("Product found: " + product);
			LOG.trace("Finished execute");
			Collection<Brand> brands = brandRepository.findAll();
			request.setAttribute("brands", brands);
			request.setAttribute("genders", Gender.rusValues);
			request.setAttribute("sizes", Size.values());
			byte[] image = ProductUtils.getProductImage(product.getName() + ".png");
			request.setAttribute("savedImage", Base64.getEncoder().encodeToString(image));
			return SUCCESS;
		} else {
			LOG.error("Product with such id not exists:" + productId);
			LOG.trace("Finished execute");
			return INPUT;
		}

	}
}
