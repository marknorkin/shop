package ua.khpi.norkin.shop.utils;

import org.apache.log4j.Logger;

public final class StringUtils {

	private static final Logger LOG = Logger.getLogger(StringUtils.class);

	public static boolean isNullOrEmpty(String value) {
		return value == null || value.isEmpty();
	}

	public static long getPositiveLong(String value) {
		try {
			long id = Long.parseLong(value);
			return id;
		} catch (NumberFormatException e) {
			LOG.error(e);
		}
		return -1;
	}
}
