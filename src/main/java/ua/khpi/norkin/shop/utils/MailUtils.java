package ua.khpi.norkin.shop.utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Client;
import ua.khpi.norkin.shop.model.Employee;

public class MailUtils {
	private static final Logger LOG = Logger.getLogger(MailUtils.class);
	private static final Session SESSION = init();
	private static final String confirmSubject = "Подтвердите регистрацию";
	private final static String confirmationClientURL = "http://localhost:8080/shop/client/confirm_registration?ID=";
	private final static String confirmationEmployeeURL = "http://localhost:8080/shop/employee/confirm_registration?ID=";

	private static Session init() {
		Session session = null;
		try {
			Context initialContext = new InitialContext();
			session = (Session) initialContext
					.lookup("java:comp/env/mail/Session");
		} catch (Exception ex) {
			LOG.error("mail session lookup error", ex);
		}
		return session;
	}

	public static void sendConfirmationEmail(Client client) {
		send(client.getFirstName(), client.getLastName(), client.getEmail(),
				client.getPassword(), true);
	}

	public static void sendConfirmationEmail(Employee employee) {
		send(employee.getFirstName(), employee.getLastName(),
				employee.getEmail(), employee.getPassword(), false);
	}

	private static void send(String firstName, String lastName, String email,
			String password, boolean isClient) {
		try {
			Message msg = new MimeMessage(SESSION);
			msg.setFrom(new InternetAddress(
					"mail.university.admission@gmail.com"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
					email));

			msg.setSubject(confirmSubject);

			Multipart multipart = new MimeMultipart();

			String encodedEmail = new String(Base64.getEncoder().encode(
					email.getBytes(StandardCharsets.UTF_8)));

			InternetHeaders emailAndPass = new InternetHeaders();
			emailAndPass.addHeader("Content-type", "text/plain; charset=UTF-8");
			String hello = "Здравствуйте, " + firstName + " " + lastName
					+ " !\n"
					+ "Вы успешно прошли регистрацию на нашем сайте.\n\n\n";

			String data = "\nВаш логин: " + email + "\nВаш пароль: " + password
					+ "\n\n";

			MimeBodyPart greetingAndData = new MimeBodyPart(emailAndPass,
					(hello + data).getBytes("UTF-8"));

			InternetHeaders headers = new InternetHeaders();
			headers.addHeader("Content-type", "text/html; charset=UTF-8");
			String confirmLink = null;
			if (isClient) {
				confirmLink = "Подтвердите регистрацию кликнув по этой "
						+ "<a href='" + confirmationClientURL + encodedEmail
						+ "'>ссылке</a>";
			} else {
				confirmLink = "Подтвердите регистрацию кликнув по этой "
						+ "<a href='" + confirmationEmployeeURL + encodedEmail
						+ "'>ссылке</a>";

			}
			MimeBodyPart link = new MimeBodyPart(headers,
					confirmLink.getBytes(StandardCharsets.UTF_8));

			multipart.addBodyPart(greetingAndData);
			multipart.addBodyPart(link);

			msg.setContent(multipart);
			msg.setSentDate(new Date());

			Transport.send(msg);
		} catch (AddressException e) {
			LOG.error(e);
		} catch (MessagingException e) {
			LOG.error(e);
		} catch (UnsupportedEncodingException e) {
			LOG.error(e);
		}
	}
}
