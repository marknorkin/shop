package ua.khpi.norkin.shop.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ua.khpi.norkin.shop.model.Product;

public class ProductUtils {
	private static final Logger LOG = Logger.getLogger(ProductUtils.class);
	public static final String PRODUCT_PATH_PART_1 = System.getProperty("user.home");
	public static final String PRODUCT_PATH_PART_2 = "/shop/data/product/";

	public static String saveProductImage(Product product, File file) throws IOException {
		LOG.trace("Start saving");
		String destDirs = PRODUCT_PATH_PART_1 + PRODUCT_PATH_PART_2 + product.getName() + ".png";
		File destDirectory = new File(destDirs);
		FileInputStream stream = new FileInputStream(file);
		FileUtils.copyInputStreamToFile(stream, destDirectory);
		LOG.trace("End saving");
		return destDirs;
	}

	public static boolean deleteProductImage(String name) {
		return FileUtils.deleteQuietly(new File(PRODUCT_PATH_PART_1 + PRODUCT_PATH_PART_2 + name + ".png"));
	}

	public static byte[] getProductImage(String path) throws IOException {
		File file = FileUtils.getFile(PRODUCT_PATH_PART_1 + PRODUCT_PATH_PART_2 + path);
		byte[] data = FileUtils.readFileToByteArray(file);
		return data;
	}

	public static String renameProductImage(String oldName, String newName) throws IOException {
		FileUtils.moveFile(new File(PRODUCT_PATH_PART_1 + PRODUCT_PATH_PART_2 + oldName + ".png"), new File(
				PRODUCT_PATH_PART_1 + PRODUCT_PATH_PART_2 + newName + ".png"));
		return PRODUCT_PATH_PART_1 + PRODUCT_PATH_PART_2 + newName + ".png";
	}

	public static void main(String[] args) throws IOException {
		ArrayList<File> listFiles = new ArrayList<File>();
		for (int i = 1; i <= 10; i++) {
			File file = FileUtils.getFile("D:/something/" + i + ".jpg");
			listFiles.add(file);
		}
		System.out.println(listFiles);

		for (int i = 0; i <= 9; i++) {
			for (int j = 0; j <= 10 * 10; j = j + 10) {
				saveImage("Shirt" + (i + j), listFiles.get(i));
			}
		}
	}

	private static String saveImage(String name, File file) throws IOException {
		LOG.trace("Start saving");
		String destDirs = PRODUCT_PATH_PART_1 + PRODUCT_PATH_PART_2 + name + ".png";
		File destDirectory = new File(destDirs);
		FileInputStream stream = new FileInputStream(file);
		FileUtils.copyInputStreamToFile(stream, destDirectory);
		LOG.trace("End saving");
		return destDirs;
	}
}
