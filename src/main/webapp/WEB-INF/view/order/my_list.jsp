<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<div class="my-orders well">
			<table id="myOrdersTable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<td><s:text name="order.my_list.id"></s:text></td>
						<td><s:text name="order.my_list.date_order"></s:text></td>
						<td><s:text name="order.my_list.date_send"></s:text></td>
						<td><s:text name="order.my_list.date_payment"></s:text></td>
						<td><s:text name="order.my_list.sum"></s:text></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="order" items="${myOrders}">
						<tr>
							<td><c:out value="${order.id}"></c:out></td>
							<td><date:convert millis="${order.dateOrder}" /></td>
							<td><date:convert millis="${order.dateSend}" /></td>
							<td><date:convert millis="${order.datePayment}" /></td>
							<td><c:out value="${order.sum}" /></td>
							<td><a href="/shop/order/view?id=${order.id}"><s:text
										name="order.my_list.view_full" /></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#myOrdersTable').dataTable();
		});
	</script>

	<script type="text/javascript"
		src="/shop/webjars/datatables/1.10.5/js/jquery.dataTables.min.js"></script>
</body>
</html>