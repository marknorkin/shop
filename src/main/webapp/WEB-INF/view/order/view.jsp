<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<div class="order-view well">

			<div class="order-view-header">

				<div class="order-view-header-item">
					<label><s:text name="order.view.date_order" /></label>
					<date:convert millis="${order.dateOrder}" />
				</div>
				<div class="order-view-header-item">
					<label><s:text name="order.view.date_send" /></label>
					<date:convert millis="${order.dateSend}" />
				</div>
				<div class="order-view-header-item">
					<label><s:text name="order.view.date_payment" /></label>
					<date:convert millis="${order.datePayment}" />
				</div>
			</div>
			<table id="orderBodyTable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<td><s:text name="order.view.product.id" /></td>
						<td><s:text name="order.view.product.name" /></td>
						<td><s:text name="order.view.product.quantity" /></td>
						<td><s:text name="order.view.product.sell_price" /></td>
						<td><s:text name="order.view.product.sum" /></td>
					</tr>
				</thead>
				<tbody>
					<c:set var="totalSum" value="${0}"></c:set>
					<c:forEach var="productBean" items="${productBeans}">
						<tr>
							<td><c:out value="${productBean.id}" /></td>
							<td><c:out value="${productBean.name}" /></td>
							<td><c:out value="${productBean.quantityLeft}" /></td>
							<td><c:out value="${productBean.sellPrice}"></c:out></td>
							<td><c:out value="${productBean.sum}" /></td>
						</tr>
						<c:set var="totalSum" value="${totalSum + productBean.sum}"></c:set>
					</c:forEach>
				</tbody>
			</table>

			<label style="text-align: right;"><s:text
					name="order.view.sum" /></label>
			<c:out value="${totalSum}" />
			<s:text name="currency.uah" />

		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#orderBodyTable').dataTable();
		});
	</script>

	<script type="text/javascript"
		src="/shop/webjars/datatables/1.10.5/js/jquery.dataTables.min.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"> -->
</body>
</html>