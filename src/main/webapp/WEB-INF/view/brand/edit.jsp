<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<div class="well form">
			<s:form action="edit" method="POST" namespace="/brand"
				cssClass="well form-search form">
				<s:textfield name="name" key="brand.name" type="text"
					required="true"></s:textfield>
				<s:submit name="button.submit" key="brand.button.submit"
					cssClass="btn btn-primary"></s:submit>
			</s:form>
		</div>
	</div>
</body>
</html>