<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<div class="well form">
			<p>
				<label for="id"><s:text name="brand.view.id" /></label>
				<c:out value="${requestScope.brand.id}"></c:out>
			</p>
			<p>
				<label for="name"><s:text name="brand.view.name" /></label>
				<c:out value="${requestScope.brand.name}"></c:out>
			</p>
			<p>
				<s:a action="edit_display" namespace="/brand"
					cssClass="btn btn-info glyphicon glyphicon-pencil">&nbsp;<s:text name="brand.view.button.edit" />
				</s:a>
			</p>
		</div>
	</div>
</body>
</html>