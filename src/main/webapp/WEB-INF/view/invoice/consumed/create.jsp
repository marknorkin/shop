<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<div class="well form-receipt">
			<form data-bind="submit: addNew">
				<span class="invoice-receipt-add-product"> <s:textfield
						name="productId" key="receipt.productId" type="number" step="0.01"
						min="0" required="true"
						data-bind='value:productId, valueUpdate: "afterkeydown"'></s:textfield>
				</span>
				<span class="invoice-receipt-add-product"> <s:textfield
						name="invoice.consumed.receipt.id" key="invoice.consumed.receipt.id" type="number" step="0.01"
						min="0" required="true"
						data-bind='value:sellPrice, valueUpdate: "afterkeydown"'></s:textfield></span>
				<span class="invoice-receipt-add-product"> <s:textfield
						name="quantity" key="receipt.quantity" type="number" step="1"
						min="0" required="true"
						data-bind='value:quantity, valueUpdate: "afterkeydown"'></s:textfield></span>
				<%-- 	<s:a cssClass="btn btn-info" ><s:text name="receipt.button.add_new"></s:text></s:a> --%>
				<s:submit name="button.submit" key="receipt.button.add_new"
					cssClass="btn btn-info"></s:submit>
			</form>

			<s:form action="create" namespace="/receipts" method="POST"
				data-bind="submit: convToJson">
				<span class="invoice-receipt-employee"> <label><s:text
							name="receipt.employee" /></label> <c:out
						value="${employee.firstName} ${employee.lastName}"></c:out>
				</span>
				<label><s:text name="receipt.type"></s:text></label>
				<select name="receiptType">
					<c:forEach items="${consumedTypes}" var="consumedType">
						<option value="${consumedType.valueRus}"><c:out
								value="${consumedType.valueRus}" /></option>
					</c:forEach>
				</select>

				<table class="table table-hover">
					<thead>
						<tr>
							<th><s:text name="receipt.productId"></s:text></th>
							<th><s:text name="invoice.consumed.receipt.id"></s:text></th>
							<th><s:text name="receipt.quantity"></s:text></th>
							<th></th>
						</tr>
					</thead>
					<tbody data-bind="foreach: bodies">
						<tr>
							<td data-bind="text:productId"></td>
							<td data-bind="text:sellPrice"></td>
							<td data-bind="text:quantity"></td>
							<td><a href="#" data-bind="click: $root.removeBody"><s:text
										name="receipt.remove"></s:text></a></td>
						</tr>
					</tbody>
				</table>
				<s:hidden name="bodyRecInv" value=""></s:hidden>
				<span data-bind="if: bodies().length > 0">
				<s:submit name="button.submit" key="receipt.button.submit"
					cssClass="btn btn-primary"></s:submit></span>
			</s:form>
		</div>
	</div>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.3.0/knockout-min.js"></script>
	<script type="text/javascript">
		// Class to represent a row in the receipt invoice grid
		function BodyReceiptInvoice(productId, sellPrice, quantity) {
			var self = this;
			self.productId = productId;
			self.sellPrice = sellPrice;
			self.quantity = quantity;
		}

		// Overall viewmodel for this screen, along with initial state
		var ReceiptInvoiceViewModel = function ReceiptInvoiceViewModel() {
			var self = this;

			this.productId = ko.observable("");
			this.sellPrice = ko.observable("");
			this.quantity = ko.observable("");

			// Editable data
			this.bodies = ko.observableArray([]);

			self.addNew = function() {
				self.bodies.push(new BodyReceiptInvoice(self.productId(), self
						.sellPrice(), self.quantity()));
				this.productId("");
				this.sellPrice("");
				this.quantity("");
			}

			self.removeBody = function(body) {
				self.bodies.remove(body)
			}

			self.convToJson = function() {
				var jsonBodies = ko.toJSON(self.bodies);
				$('input[name=bodyRecInv]').val(jsonBodies);
				return true;
			}
		}

		ko.applyBindings(new ReceiptInvoiceViewModel());
	</script>
</body>
</html>