<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<div class="invoice-all well">
			<table id="consumedInvoicesTable"
				class="table table-striped table-bordered">
				<thead>
					<tr>
						<td><s:text name="invoice.consumed.list.id"></s:text></td>
						<td><s:text name="invoice.consumed.list.date_consumed"></s:text></td>
						<td><s:text name="invoice.consumed.list.consumed_type"></s:text></td>
						<td><s:text name="invoice.consumed.list.employee_id"></s:text></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="consumedInvoice" items="${consumedInvoices}">
						<tr>
							<td><c:out value="${consumedInvoice.id}"></c:out></td>
							<td><date:convert millis="${consumedInvoice.dateConsumed}" /></td>
							<td><c:out value="${consumedInvoice.consumedType.valueRus}"></c:out></td>
							<td><c:out value="${consumedInvoice.employeeId}"></c:out></td>
							<td><a href="/shop/consumed/view?id=${consumedInvoice.id}"><s:text
										name="invoice.consumed.list.view_full" /></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#consumedInvoicesTable').dataTable();
		});
	</script>

	<script type="text/javascript"
		src="/shop/webjars/datatables/1.10.5/js/jquery.dataTables.min.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"> -->
</body>
</html>