<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<div class="receipt-view well">

			<div class="receipt-view-header">

				<div class="receipt-view-header-item">
					<label><s:text name="consumed.view.date_consumed" /></label>
					12-06-15
				</div>
				<div class="receipt-view-header-item">
					<label><s:text name="consumed.view.consumed_type" /></label>
					<s:text name="sell"></s:text>
				</div>
				<div class="receipt-view-header-item">
					<label><s:text name="consumed.view.employee" /></label>
					${employee.firstName} ${employee.lastName}
				</div>
			</div>
			<table id="consumedBodyTable"
				class="table table-striped table-breceipted">
				<thead>
					<tr>
						<td><s:text name="consumed.view.id" /></td>
						<td><s:text name="consumed.view.product_id" /></td>
						<td><s:text name="consumed.view.quantity" /></td>
						<td><s:text name="consumed.view.receipt_invoice_id" /></td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="bodyConsumedInvoice" items="${bodyConsumedInvoices}">
						<tr>
							<td><c:out value="${bodyConsumedInvoice.id}" /></td>
							<td><c:out value="${bodyConsumedInvoice.productId}" /></td>
							<td><c:out value="${bodyConsumedInvoice.quantity}" /></td>
							<td><c:out value="${bodyConsumedInvoice.receiptInvoiceId}"></c:out> </td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<a class="btn btn-info" href="#"><s:text name="print"/></a>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#consumedBodyTable').dataTable();
		});
	</script>

	<script type="text/javascript"
		src="/shop/webjars/datatables/1.10.5/js/jquery.dataTables.min.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"> -->
</body>
</html>