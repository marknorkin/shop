<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<div class="receipt-view well">

			<div class="receipt-view-header">

				<div class="receipt-view-header-item">
					<label><s:text name="receipt.view.date_receipt" /></label>
					<date:convert millis="${receiptInvoice.dateReceipt}" />
				</div>
				<div class="receipt-view-header-item">
					<label><s:text name="receipt.view.receipt_type" /></label>
					<s:text name="purchase"></s:text>
				</div>
				<div class="receipt-view-header-item">
					<label><s:text name="receipt.view.date_payment" /></label>
					${employee.firstName} ${employee.lastName}
				</div>
			</div>
			<table id="receiptBodyTable"
				class="table table-striped table-breceipted">
				<thead>
					<tr>
						<td><s:text name="receipt.view.id" /></td>
						<td><s:text name="receipt.view.product_id" /></td>
						<td><s:text name="receipt.view.buy_price" /></td>
						<td><s:text name="receipt.view.sell_price" /></td>
						<td><s:text name="receipt.view.quantity" /></td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="bodyReceiptInvoice" items="${bodyReceiptInvoices}">
						<tr>
							<td><c:out value="${bodyReceiptInvoice.id}" /></td>
							<td><c:out value="${bodyReceiptInvoice.productId}" /></td>
							<td><c:out value="${bodyReceiptInvoice.buyPrice}" /></td>
							<td><c:out value="${bodyReceiptInvoice.sellPrice}"></c:out></td>
							<td><c:out value="${bodyReceiptInvoice.quantity}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<a class="btn btn-info" href="#"><s:text name="print"/></a>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#receiptBodyTable').dataTable();
		});
	</script>

	<script type="text/javascript"
		src="/shop/webjars/datatables/1.10.5/js/jquery.dataTables.min.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"> -->
</body>
</html>