<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<div class="invoice-all well">
			<table id="receiptInvoicesTable"
				class="table table-striped table-bordered">
				<thead>
					<tr>
						<td><s:text name="invoice.receipt.list.id"></s:text></td>
						<td><s:text name="invoice.receipt.list.date_receipt"></s:text></td>
						<td><s:text name="invoice.receipt.list.receipt_type"></s:text></td>
						<td><s:text name="invoice.receipt.list.employee_id"></s:text></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="receiptInvoice" items="${receiptInvoices}">
						<tr>
							<td><c:out value="${receiptInvoice.id}"></c:out></td>
							<td><date:convert millis="${receiptInvoice.dateReceipt}" /></td>
							<td><c:out value="${receiptInvoice.receiptType.valueRus}"></c:out></td>
							<td><c:out value="${receiptInvoice.employeeId}"></c:out></td>
							<td><a href="/shop/receipts/view?id=${receiptInvoice.id}"><s:text
										name="invoice.receipt.list.view_full" /></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#receiptInvoicesTable').dataTable();
		});
	</script>

	<script type="text/javascript"
		src="/shop/webjars/datatables/1.10.5/js/jquery.dataTables.min.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"> -->
</body>
</html>