<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<div class="product-find-search">
			<p>
				<label for="product_identifier"><s:text
						name="product.find.label.find" /> </label> <input type="text"
					id="product_identifier" name="product_identifier" required>
				<input type="button" class="btn btn-success"
					value="<s:text name="product.find.button.search"/>"
					onclick="return findProduct($('#product_identifier').val());">
			</p>
		</div>
		<br> <br>
		<div id="product" class="well product-view"></div>
	</div>

	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>

	<script id="product-template" type="text/mustache-template">
<img src="<s:url action="load?name={{name}}.png" namespace="/images"></s:url>" />

<div class="product-view-info">
		<label for="{{id}}"><s:text name="product.view.id" /> </label> {{id}}
</br>
		<label for="{{name}}"><s:text name="product.view.name" /> </label> {{name}}
</br>
		<label for="{{description}}"><s:text name="product.view.description" /> </label> {{description}}
</br>
		<label for="{{gender}}"><s:text name="product.view.gender" /> </label> {{gender}}
</br>
		<label for="{{size}}"><s:text name="product.view.size" /> </label> {{size}}
</br>
		<label for="{{brand}}"><s:text name="product.view.brand" /> </label> {{brand}}
</br>
<a href="/shop/product/edit?id={{id}}" class="btn btn-warning" role="button"><s:text name="product.view.button.edit" /></a>
</div>
	</script>

	<script type="text/javascript">
		/**
		 * Finds publications by product name or id
		 */
		function findProduct(identifier) {
			$.ajax({
				type : 'GET',
				dataType : 'json',
				url : '/shop/product/find?productIdentifier=' + identifier,
				success : function(data) {

					if (data.msg) {
						$('#product').text(data.msg);
					} else {
						var product = data.product;
						product["brand"] = data.brand.name;
						var template = $('#product-template').html();
						var info = Mustache.render(template, product);
						$('#product').html(info);
					}
				}
			})
		}
	</script>
</body>
</html>