<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<div class="product-list">
			<ul class="sync-pagination pagination"></ul>
			<div id="products"></div>
			<ul class="sync-pagination pagination"></ul>
		</div>
	</div>

	<script type="text/javascript"
		src="/shop/webjars/mustachejs/0.8.2/mustache.js"></script>
	<script id="products-template" type="text/mustache-template">
<ul class="list-group">
{{#.}}
  <li class="list-group-item product-list-item">
<img class="product-list-image" src="<s:url action="load?name={{name}}.png" namespace="/images"></s:url>" />
<div class="product-list-info">
<p>
<span class="product-list-info-field">
		<label for="{{name}}"><s:text name="product.list.name" /></label> {{name}}
</span>
</p>
<p>
<span class="product-list-info-field">
		<label for="{{description}}"><s:text name="product.list.description" /></label> {{description}}
</span>
</p>
{{^canBuy}}
<p>
<span class="product-list-info-field">
  <span class="btn btn-warning"><span class="glyphicon glyphicon-thumbs-down"/>&nbsp;<s:text name="product.list.cant_buy" /></span>
</span>
</p>
{{/canBuy}}
{{#canBuy}}
<p>
<span class="product-list-info-field">
<label for="{{sellPrice}}"><s:text name="product.list.sell_price" /></label> {{sellPrice}} <s:text name="currency.uah"/>
</span>
</p>
<p>
<span class="product-list-info-field">
<span class="btn btn-success"><span class="glyphicon glyphicon-thumbs-up"/>&nbsp;<s:text name="product.list.can_buy" /></span>
</span>
</p>
{{/canBuy}}
<p>
<s:a action="view?id={{id}}" namespace="/product"
	cssClass="btn btn-info ">&nbsp;<s:text name="product.list.open_full_message" />
</s:a>
</p>
</div>
  </li>
{{/.}}
</ul>
	</script>
	<script type="text/javascript"
		src="/shop/script/jquery.twbsPagination.min.js"></script>
	<script type="text/javascript">
		var records = "${requestScope.records}";
		var recordsPerPage = 5;
		var pages = Math.ceil(records / recordsPerPage);
		$('.sync-pagination')
				.twbsPagination(
						{
							totalPages : pages,
							visiblePages : 7,
							first : '\u041F\u0435\u0440\u0432\u0430\u044F',
							last : '\u041F\u043E\u0441\u043B\u0435\u0434\u043D\u044F\u044F',
							prev : '\u041F\u0440\u0435\u0434\u044B\u0434\u0443\u0449\u0430\u044F',
							next : '\u0421\u043B\u0435\u0434\u0443\u044E\u0449\u0430\u044F',
							onPageClick : function(event, page) {
								$('#products').html(changePage(page));
							}
						});

		function changePage(page) {
			pnumber = page || 1;

			$.ajax({
				type : 'GET',
				dataType : 'json',
				url : '/shop/product/upload_products?page=' + pnumber,
				success : function(products) {
					var template = $('#products-template').html();
					var info = Mustache.render(template, products);
					$('#products').html(info);
				}
			})
		}

		//to load for the first time
		changePage();
	</script>
</body>
</html>