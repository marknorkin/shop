<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<s:form action="add" method="POST" namespace="/product"
			cssClass="well form-search form" enctype="multipart/form-data">
			<s:textfield name="name" key="product.add.name" type="text"
				required="true"></s:textfield>
			<s:textfield name="description" key="product.add.description"
				type="text" required="true"></s:textfield>
			<%-- <span class="file-input btn btn-block btn-primary btn-file"> <s:file
					name="image" key="product.add.image" />
			</span> --%>
			<!-- image-preview-filename input [CUT FROM HERE]-->
			<div class="input-group image-preview">
				<input type="text" class="form-control image-preview-filename"
					disabled="disabled">
				<!-- don't give a name === doesn't send on POST/GET -->
				<span class="input-group-btn"> <!-- image-preview-clear button -->
					<button type="button" class="btn btn-default image-preview-clear"
						style="display: none;">
						<span class="glyphicon glyphicon-remove"></span><s:text name="product.add.image.clear"/>
					</button> <!-- image-preview-input -->
					<div class="btn btn-info image-preview-input">
						<span class="glyphicon glyphicon-folder-open"></span> <span
							class="image-preview-input-title"><s:text name="product.add.image.choose"></s:text></span> <input
							type="file" accept="image/png, image/jpeg, image/gif"
							name="image" />
						<!-- rename it -->
					</div>
				</span>
			</div>
			<!-- /input-group image-preview [TO HERE]-->
			<br>
			<label><s:text name="product.add.brand" /></label>
			<select name="brandId" class="form-control">
				<c:forEach items="${brands}" var="brand">
					<option value="${brand.id}"><c:out value="${brand.name}" /></option>
				</c:forEach>
			</select>
			<br>
			<label><s:text name="product.add.gender" /></label>
			<select name="genderValue" class="form-control">
				<c:forEach items="${genders}" var="gender" varStatus="loop">
					<option value="${gender}" ${loop.index eq -1 ? 'selected=="true"': ''}><c:out value="${gender}" /></option>
				</c:forEach>
			</select>
			<br>
			<label><s:text name="product.add.size" /></label>
			<select name="sizeValue" class="form-control">
				<c:forEach items="${sizes}" var="size">
					<option value="${size}"><c:out value="${size}" /></option>
				</c:forEach>
			</select>
			<br>
			<s:submit name="button.submit" key="product.add.button.submit"
				cssClass="btn btn-primary"></s:submit>
		</s:form>
	</div>
	<script type="text/javascript">
		$(document).on('click', '#close-preview', function() {
			$('.image-preview').popover('hide');
			// Hover befor close the preview
			$('.image-preview').hover(function() {
				$('.image-preview').popover('show');
			}, function() {
				$('.image-preview').popover('hide');
			});
		});

		$(function() {
			// Create the close button
			var closebtn = $('<button/>', {
				type : "button",
				text : 'x',
				id : 'close-preview',
				style : 'font-size: initial;',
			});
			closebtn.attr("class", "close pull-right");
			// Set the popover default content
			$('.image-preview').popover({
				trigger : 'manual',
				html : true,
				title : "<strong>Preview</strong>" + $(closebtn)[0].outerHTML,
				content : "There's no image",
				placement : 'bottom'
			});
			// Clear event
			$('.image-preview-clear').click(function() {
				$('.image-preview').attr("data-content", "").popover('hide');
				$('.image-preview-filename').val("");
				$('.image-preview-clear').hide();
				$('.image-preview-input input:file').val("");
				$(".image-preview-input-title").text("Browse");
			});
			// Create the preview image
			$(".image-preview-input input:file").change(
					function() {
						var img = $('<img/>', {
							id : 'dynamic',
							width : 250,
							height : 200
						});
						var file = this.files[0];
						var reader = new FileReader();
						// Set preview image into the popover data-content
						reader.onload = function(e) {
							$(".image-preview-input-title").html("<s:text name='product.add.image.change' />");
							$(".image-preview-clear").show();
							$(".image-preview-filename").val(file.name);
							img.attr('src', e.target.result);
							$(".image-preview").attr("data-content",
									$(img)[0].outerHTML).popover("show");
						}
						reader.readAsDataURL(file);
					});
		});
	</script>
</body>
</html>