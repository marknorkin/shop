<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">

		<div id="product" class="well product-view">
			<label id="name"><c:out value="${requestScope.product.name}"></c:out></label>
			<img alt="${requestScope.product.name}"
				src="<s:url action="load" namespace="/images"><s:param name="name">${requestScope.product.name}.png</s:param></s:url>" />
			<%-- <img style="width:700px;height:500px;" alt="${product.name}" src="data:image/png;base64,${image}"> --%>

			<div class="product-view-info">
				<p>
					<label for="description"><s:text
							name="product.view.description" /></label>
					<c:out value="${requestScope.product.description}"></c:out>
				</p>
				<p>
					<label for="gender"><s:text name="product.view.gender" /></label>
					<c:out value="${requestScope.product.gender.valueRus}"></c:out>
				</p>
				<p>
					<label for="size"><s:text name="product.view.size" /></label>
					<c:out value="${requestScope.product.size}"></c:out>
				</p>
				<p>
					<label for="brandId"><s:text name="product.view.brand" /></label>
					<c:out value="${requestScope.brand}"></c:out>
				</p>
				<c:choose>
					<c:when test="${not empty client}">
						<c:choose>
							<c:when test="${sellPrice gt 0}">
								<p>
									<label for="price"><s:text name="product.view.price" /></label>
									<c:out value="${requestScope.sellPrice}" />
									&nbsp;
									<s:text name="currency.uah" />
								</p>
								<p>

									<span class="btn btn-success"><span
										class="glyphicon glyphicon-thumbs-up"></span>&nbsp;<s:text
											name="product.list.can_buy" /></span>
								</p>
								<p>
									<s:a href="#"
										onclick="return AddToCart(%{#request.product.id});"
										cssClass="btn btn-primary">
										<s:text name="product.view.button.make_order" />
									</s:a>
								</p>
							</c:when>
							<c:otherwise>
								<p>
									<span class="btn btn-warning"><span
										class="glyphicon glyphicon-thumbs-down"></span>&nbsp;<s:text
											name="product.list.cant_buy" /></span>
								</p>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:when test="${not empty employee}">
						<p>
							<s:a action="edit_display?id=%{#request.product.id}"
								namespace="/product" cssClass="btn btn-warning">
								<s:text name="product.view.button.edit" />
							</s:a>
						</p>
					</c:when>
					<c:otherwise>
						<p>
							<s:a action="register_display" namespace="/client"
								cssClass="btn btn-info ">&nbsp;<s:text
									name="product.view.button.register" />
							</s:a>
						</p>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function AddToCart(productId) {
			$.post("/shop/cart/add", {
				'productId' : productId
			})

			alert("\u0422\u043E\u0432\u0430\u0440 \u0434\u043E\u0431\u0430\u0432\u043B\u0435\u043D \u0432 \u043A\u043E\u0440\u0437\u0438\u043D\u0443");
		}
	</script>
</body>
</html>