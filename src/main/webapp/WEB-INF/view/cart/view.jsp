<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<!-- TODO  -->
	<div class="container">
		<div class="well cart-view-form">
			<s:form action="make_order" namespace="/order" method="POST"
				data-bind="submit: convToJson">
				<table class="table table-hover">
					<thead>
						<tr>
							<th><s:text name="cart.product.image" /></th>
							<th><s:text name="cart.product.name" /></th>
							<th><s:text name="cart.product.size" /></th>
							<th><s:text name="cart.product.gender" /></th>
							<th><s:text name="cart.product.quantity" /></th>
							<th><s:text name="cart.product.sell_price" /></th>
							<th></th>
						</tr>
					</thead>

					<tbody data-bind="foreach: products">

						<tr>
							<td>
								<!-- <img class="cart-view-img" data-bind="attr:{ src: '/shop/images/load?name=' + imageUrl()}"/> -->
								<img class="cart-view-img" data-bind="attr:{src: imageUrl}" />
							</td>
							<td data-bind="text:name"></td>
							<td data-bind="text:size"></td>
							<td data-bind="text:gender"></td>
							<td><input type="number"
								data-bind="value: quantity, attr: {max: maxQuantity, min: 1, step: 1}">
							</td>
							<td data-bind="text:sellPrice"></td>
							<td><a href="#" data-bind="click: $root.removeProduct"><s:text
										name="cart.view.button.remove"></s:text></a></td>
						</tr>

						<%-- <c:forEach items="${clientCart}" var="productBean">
							<tr>
								<td><img class="cart-view-img" alt="${productBean.name}"
									src="<s:url action="load" namespace="/images"><s:param name="name">${productBean.name}.png</s:param></s:url>" /></td>
								<td><c:out value="${productBean.name}" /></td>
								<td><c:out value="${productBean.size}" /></td>
								<td><c:out value="${productBean.gender.valueRus}" /></td>
								<td><input type="number" value="1"
									max="${productBean.quantityLeft}" min="1" step="1" required></td>
								<td><c:out value="${productBean.sellPrice}" />&nbsp;<s:text
										name="currency.uah" /></td>
							</tr>
						</c:forEach> --%>
					</tbody>
				</table>
				<c:if test="${fn:length(clientCart) gt 0}">
					<s:submit name="button.submit" key="cart.button.submit"
						cssClass="btn btn-primary"></s:submit>
				</c:if>

				<s:hidden name="cartInJson" value=""></s:hidden>
				<span data-bind="if: products().length > 0"> <s:submit
						name="button.submit" key="cart.button.submit"
						cssClass="btn btn-primary"></s:submit>
				</span>
			</s:form>
		</div>
	</div>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.3.0/knockout-min.js"></script>
	<script type="text/javascript">
		function ProductBean(bean) {
			this.id = ko.observable(bean.id);
			this.imageUrl = ko.observable('/shop/images/load?name=' + bean.name
					+ '.png');
			this.name = ko.observable(bean.name);
			this.size = ko.observable(bean.size);
			this.gender = ko.observable(bean.gender);
			this.maxQuantity = bean.quantityLeft;
			this.quantity = ko.observable(1)
			this.sellPrice = ko.observable(bean.sellPrice);
			this.receiptInvoiceId = ko.observable(bean.receiptInvoiceId);
		}

		function ProductBeanViewModel() {
			// Data
			var self = this;
			self.products = ko.observableArray([]);
			self.removeFromCart = function(product) {
				self.products.remove(product)
			};

			// Load initial state from server, convert it to Task instances, then populate self.tasks
			$.getJSON("/shop/cart/data", function(productBeans) {
				var mappedProducts = $.map(productBeans, function(productBean) {
					return new ProductBean(productBean)
				});
				self.products(mappedProducts);
			});

			self.removeProduct = function(product) {
				$.post("/shop/cart/remove", {
					"productId" : product.id
				});
				self.products.remove(product)
			}

			self.convToJson = function() {
				var jsonProducts = ko.toJSON(self.products);
				$('input[name=cartInJson]').val(jsonProducts);
				return true;
			}

		}

		ko.applyBindings(new ProductBeanViewModel());
	</script>
</body>
</html>