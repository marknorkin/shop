<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<table id="positionsTable" class="table table-striped table-bordered">
			<thead>
				<tr>
					<td><s:text name="position.list.id"></s:text></td>
					<td><s:text name="position.list.name"></s:text></td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="position" items="${positions}">
					<tr>
						<td><c:out value="${position.id}"></c:out></td>
						<td><s:a action="view?#{position.id}" namespace="/brand">
								<c:out value="${position.name}"></c:out>
							</s:a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		var language = "${language}";
		$(document).ready(function() {
			$('#positionsTable').dataTable();
		});
	</script>

	<script type="text/javascript" src="/shop/webjars/datatables/1.10.5/js/jquery.dataTables.min.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"> -->
</body>
</html>