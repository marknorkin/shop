<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<div class="form">
			<s:form action="add" method="POST" namespace="/position"
				cssClass="well form-search">
				<s:textfield name="name" key="position.add.name" type="text"
					required="true"></s:textfield>
				<s:submit name="button.submit" key="position.button.submit"
					cssClass="btn btn-primary"></s:submit>
			</s:form>
		</div>
	</div>
</body>
</html>