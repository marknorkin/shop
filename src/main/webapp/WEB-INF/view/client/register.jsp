<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<s:form action="register" method="POST" namespace="/client"
			cssClass="well form-search form">
			<s:textfield name="firstName" key="register.name" type="text"
				required="true"></s:textfield>
			<s:textfield name="lastName" key="register.surname" type="text"
				required="true"></s:textfield>
			<s:textfield name="email" key="register.email" type="email"
				required="true"></s:textfield>
			<s:password name="password" key="register.password" type="password"
				required="true"></s:password>
			<label><s:text name="register.adress"></s:text></label>
			<div class="adress">
				<s:textfield name="city" key="register.city" type="text"
					required="true"></s:textfield>
				<s:textfield name="street" key="register.street" type="text"
					required="true"></s:textfield>
				<s:textfield name="house" key="register.house" type="text"
					required="true"></s:textfield>
				<s:textfield name="appartement" key="register.appartement"
					type="text" required="true"></s:textfield>
			</div>
			<s:textfield name="phone" key="register.phone" type="text"
				required="true"></s:textfield>
			<s:submit name="button.submit" key="register.button.submit"
				cssClass="btn btn-primary"></s:submit>
		</s:form>
	</div>
</body>
</html>