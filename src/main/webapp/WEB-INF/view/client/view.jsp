<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<div class="well form">
			<p>
				<label for="firstName"><s:text name="profile.view.name" /></label>
				<c:out value="${requestScope.client.firstName}"></c:out>
			</p>
			<p>
				<label for="lastName"><s:text name="profile.view.surname" /></label>
				<c:out value="${requestScope.client.lastName}"></c:out>
			</p>
			<p>
				<label for="email"><s:text name="profile.view.email" /></label>
				<c:out value="${requestScope.client.email}"></c:out>
			</p>
			<p>
				<label for="password"><s:text name="profile.view.password" /></label>
				<c:out value="****"></c:out>
			</p>
			<p>
				<label class="text-primary" style="margin: auto; width: 40%;"><s:text
						name="profile.edit.adress" /></label>
			</p>
			<p>
				<label for="city"><s:text name="profile.view.city" /></label>
				<c:out value="${requestScope.client.city}"></c:out>
			</p>
			<p>
				<label for="street"><s:text name="profile.view.street" /></label>
				<c:out value="${requestScope.client.street}"></c:out>
			</p>

			<p>
				<label for="house"><s:text name="profile.view.house" /></label>
				<c:out value="${requestScope.client.house}"></c:out>
			</p>

			<p>
				<label for="appartement"><s:text
						name="profile.view.appartement" /></label>
				<c:out value="${requestScope.client.appartement}"></c:out>
			</p>

			<p>
				<label for="phone"><s:text name="profile.view.phone" /></label>
				<c:out value="${requestScope.client.phone}"></c:out>
			</p>
			<p>
				<s:a action="edit_display" namespace="/client"
					cssClass="btn btn-info glyphicon glyphicon-pencil">&nbsp;<s:text
						name="profile.view.button.edit" />
				</s:a>
			</p>
			<p>
				<s:a action="my_list" namespace="/order"
					cssClass="btn btn-primary glyphicon glyphicon-file">&nbsp;<s:text
						name="profile.view.button.my_orders" />
				</s:a>
			</p>
		</div>
	</div>
</body>
</html>