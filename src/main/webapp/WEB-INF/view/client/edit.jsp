<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<s:form action="edit" method="POST" namespace="/client"
			cssClass="well form">

			<div class="form-group">
				<s:textfield name="firstName" key="profile.edit.name" type="text"
					value="%{#session.client.firstName}" required="true"></s:textfield>
			</div>
			<div class="form-group">
				<s:textfield name="lastName" key="profile.edit.surname" type="text"
					value="%{#session.client.lastName}"></s:textfield>
			</div>
			<div class="form-group">
				<s:textfield name="email" key="profile.edit.email" type="email"
					value="%{#session.client.email}" required="true" readonly="true"></s:textfield>
			</div>
			<div class="form-group">
				<s:password name="password" key="profile.edit.password"
					type="password" value="%{#session.client.password}" required="true"></s:password>
			</div>
			<label class="text-primary"><s:text name="profile.edit.adress"></s:text></label>
			<div class="form-group">
				<s:textfield name="city" key="profile.edit.city" type="text"
					value="%{#session.client.city}"></s:textfield>
			</div>
			<div class="form-group">
				<s:textfield name="street" key="profile.edit.street" type="text"
					value="%{#session.client.street}"></s:textfield>
			</div>
			<div class="form-group">
				<s:textfield name="house" key="profile.edit.house" type="text"
					value="%{#session.client.house}"></s:textfield>
			</div>
			<div class="form-group">
				<s:textfield name="appartement" key="profile.edit.appartement"
					type="text" value="%{#session.client.appartement}"></s:textfield>
			</div>
			<div class="form-group">
				<s:textfield name="phone" key="profile.edit.phone" type="text"
					value="%{#session.client.phone}"></s:textfield>
			</div>
			<div class="form-group">
				<s:submit key="profile.edit.button.submit"
					cssClass="btn btn-primary"></s:submit>
			</div>
			<s:a action="view" namespace="/client" cssClass="btn btn-info">
				<s:text name="profile.edit.button.back" />
			</s:a>
		</s:form>
	</div>
</body>
</html>