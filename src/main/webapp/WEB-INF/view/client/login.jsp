<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<s:form action="login" method="POST" namespace="/user"
			cssClass="well form-search form" >
			<div class="form-group">
				<s:textfield name="email" key="login.email" type="email"></s:textfield>
			</div>
			<div class="form-group">
				<s:password name="password" key="login.password" type="password"></s:password>
			</div>
			<div class="form-group">
				<s:submit name="submit" key="login.button.submit"
					cssClass="btn btn-primary"></s:submit>
			</div>
		</s:form>
	</div>
</body>
</html>
