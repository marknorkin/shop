<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<div class="well form">
			<p>
				<label for="firstName"><s:text name="profile.view.name" /></label>
				<c:out value="${sessionScope.employee.firstName}"></c:out>
			</p>
			<p>
				<label for="lastName"><s:text name="profile.view.surname" /></label>
				<c:out value="${sessionScope.employee.lastName}"></c:out>
			</p>
			<p>
				<label for="email"><s:text name="profile.view.email" /></label>
				<c:out value="${sessionScope.employee.email}"></c:out>
			</p>
			<p>
				<label for="password"><s:text name="profile.view.password" /></label>
				<c:out value="****"></c:out>
			</p>
			<p>
				<label for="position"><s:text name="profile.view.position" /></label>
				<c:out value="${sessionScope.position.name}"></c:out>
			</p>

			<p>
				<s:a action="edit_display" namespace="/employee"
					cssClass="btn btn-info glyphicon glyphicon-pencil">&nbsp;<s:text
						name="profile.view.button.edit" />
				</s:a>
			</p>
		</div>
	</div>
</body>
</html>