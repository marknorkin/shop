<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>

	<div class="container">
		<s:form action="register" method="POST" namespace="/employee"
			cssClass="well form-search form">
			<s:textfield name="firstName" key="register.name" type="text"
				required="true"></s:textfield>
			<s:textfield name="lastName" key="register.surname" type="text"
				required="true"></s:textfield>
			<s:textfield name="email" key="register.email" type="email"
				required="true"></s:textfield>
			<s:password name="password" key="register.password" type="password"
				required="true"></s:password>

			<s:combobox list="#{positions}"></s:combobox>
			<s:submit name="button.submit" key="register.button.submit"
				cssClass="btn btn-primary"></s:submit>
		</s:form>
	</div>
</body>
</html>