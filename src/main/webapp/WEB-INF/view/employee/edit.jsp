<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>
	<div class="container">
		<s:form action="edit" method="POST" namespace="/employee"
			cssClass="well form">

			<div class="form-group">
				<s:textfield name="firstName" key="profile.edit.name" type="text"
					value="%{#session.employee.firstName}" required="true"></s:textfield>
			</div>
			<div class="form-group">
				<s:textfield name="lastName" key="profile.edit.surname" type="text"
					value="%{#session.employee.lastName}"></s:textfield>
			</div>
			<div class="form-group">
				<s:textfield name="email" key="profile.edit.email" type="email"
					value="%{#session.employee.email}" required="true" readonly="true"></s:textfield>
			</div>
			<div class="form-group">
				<s:password name="password" key="profile.edit.password"
					type="password" value="%{#session.employee.password}"
					required="true"></s:password>
			</div>
			<div class="form-group">
				<s:textfield name="position" key="profile.edit.position" type="text"
					value="%{#session.position.name}" required="true" readonly="true"></s:textfield>
			</div>
			<div class="form-group">
				<s:submit key="profile.edit.button.submit"
					cssClass="btn btn-primary"></s:submit>
			</div>

			<s:a action="view" namespace="/employee" cssClass="btn btn-info">
				<s:text name="profile.edit.button.back" />
			</s:a>
		</s:form>
	</div>
</body>
</html>