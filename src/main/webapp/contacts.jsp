<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/jspf/menu.jspf"%>


	<div class="container">
		<div class="payment-and-shipment-form">
			<h3>
				<s:text name="common.contacts" />
			</h3>
			<pre>
			<c:import charEncoding="UTF-8" url="/files/contacts.txt" />
		</pre>
		</div>
	</div>
</body>
</html>
