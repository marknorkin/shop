SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `shop` ;
CREATE SCHEMA IF NOT EXISTS `shop` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `shop` ;

-- -----------------------------------------------------
-- Table `shop`.`client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`client` ;

CREATE TABLE IF NOT EXISTS `shop`.`client` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `house` VARCHAR(45) NOT NULL,
  `appartement` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(15) NOT NULL,
  `is_active` TINYINT(1) NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `телефон_UNIQUE` (`phone` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`brand`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`brand` ;

CREATE TABLE IF NOT EXISTS `shop`.`brand` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`product` ;

CREATE TABLE IF NOT EXISTS `shop`.`product` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `image_url` VARCHAR(150) NOT NULL,
  `description` VARCHAR(400) NOT NULL,
  `gender` ENUM('male','female') NOT NULL,
  `size` ENUM('XS','S','M','L','XL','XXL') NOT NULL,
  `brand_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `image_UNIQUE` (`image_url` ASC),
  INDEX `fk_product_brand1_idx` (`brand_id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  CONSTRAINT `fk_brand`
    FOREIGN KEY (`brand_id`)
    REFERENCES `shop`.`brand` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`position`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`position` ;

CREATE TABLE IF NOT EXISTS `shop`.`position` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`employee` ;

CREATE TABLE IF NOT EXISTS `shop`.`employee` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `is_active` TINYINT(1) NOT NULL DEFAULT false,
  `position_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_user_role1_idx` (`position_id` ASC),
  CONSTRAINT `fk_user_role1`
    FOREIGN KEY (`position_id`)
    REFERENCES `shop`.`position` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`order` ;

CREATE TABLE IF NOT EXISTS `shop`.`order` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` BIGINT UNSIGNED NOT NULL,
  `date_order` BIGINT NOT NULL,
  `date_send` BIGINT NULL,
  `date_payment` BIGINT NULL,
  `sum` DECIMAL NOT NULL,
  `id_express_invoice` BIGINT NULL,
  `consumed_invoice_id` DECIMAL NOT NULL,
  `employee_id` BIGINT UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_product_has_client_client1_idx` (`client_id` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `id_экспресс_накладной_UNIQUE` (`id_express_invoice` ASC),
  INDEX `fk_order_employee1_idx` (`employee_id` ASC),
  CONSTRAINT `fk_product_has_client_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `shop`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_employee1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `shop`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`cart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`cart` ;

CREATE TABLE IF NOT EXISTS `shop`.`cart` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` BIGINT UNSIGNED NOT NULL,
  `order_id` BIGINT UNSIGNED NOT NULL,
  `quantity` BIGINT UNSIGNED NOT NULL,
  `receipt_invoice_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_product_has_order_order1_idx` (`order_id` ASC),
  INDEX `fk_product_has_order_product1_idx` (`product_id` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_product_has_order_product1`
    FOREIGN KEY (`product_id`)
    REFERENCES `shop`.`product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_has_order_order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `shop`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`receipt_invoice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`receipt_invoice` ;

CREATE TABLE IF NOT EXISTS `shop`.`receipt_invoice` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_receipt` BIGINT NOT NULL,
  `receipt_type` ENUM('return','purchase') NOT NULL,
  `employee_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_prihod_nakl_employee1_idx` (`employee_id` ASC),
  CONSTRAINT `fk_prihod_nakl_employee1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `shop`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`body_receipt_invoice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`body_receipt_invoice` ;

CREATE TABLE IF NOT EXISTS `shop`.`body_receipt_invoice` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` BIGINT UNSIGNED NOT NULL,
  `receipt_invoice_id` BIGINT UNSIGNED NOT NULL,
  `buy_price` DECIMAL UNSIGNED NOT NULL,
  `sell_price` DECIMAL UNSIGNED NOT NULL,
  `quantity` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_body_receipt_invoice_receipt_invoice` (`receipt_invoice_id` ASC),
  INDEX `fk_body_receipt_invoice_product` (`product_id` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_body_receipt_invoice_product`
    FOREIGN KEY (`product_id`)
    REFERENCES `shop`.`product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_body_receipt_invoice_receipt_invoice`
    FOREIGN KEY (`receipt_invoice_id`)
    REFERENCES `shop`.`receipt_invoice` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`consumed_invoice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`consumed_invoice` ;

CREATE TABLE IF NOT EXISTS `shop`.`consumed_invoice` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_consumed` BIGINT NOT NULL,
  `consumed_type` ENUM('sold','theft','waste') NOT NULL,
  `is_consumed` TINYINT(1) NOT NULL DEFAULT false,
  `employee_id` BIGINT UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_rashod_nakl_employee1_idx` (`employee_id` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_rashod_nakl_employee1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `shop`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shop`.`body_consumed_invoice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `shop`.`body_consumed_invoice` ;

CREATE TABLE IF NOT EXISTS `shop`.`body_consumed_invoice` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` BIGINT UNSIGNED NOT NULL,
  `consumed_invoice_id` BIGINT UNSIGNED NOT NULL,
  `quantity` BIGINT UNSIGNED NOT NULL,
  `receipt_invoice_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_body_consumed_invoice_consumed_invoice` (`consumed_invoice_id` ASC),
  INDEX `fk_body_consumed_invoice_product` (`product_id` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_body_consumed_invoice_product`
    FOREIGN KEY (`product_id`)
    REFERENCES `shop`.`product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_body_consumed_invoice_consumed_invoice`
    FOREIGN KEY (`consumed_invoice_id`)
    REFERENCES `shop`.`consumed_invoice` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `shop` ;

-- -----------------------------------------------------
-- Placeholder table for view `shop`.`male_products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`male_products` (`id` INT, `name` INT, `image_url` INT, `description` INT, `gender` INT, `size` INT, `brand_id` INT);

-- -----------------------------------------------------
-- Placeholder table for view `shop`.`female_products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`female_products` (`id` INT, `name` INT, `image_url` INT, `description` INT, `gender` INT, `size` INT, `brand_id` INT);

-- -----------------------------------------------------
-- Placeholder table for view `shop`.`warehouse`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`warehouse` (`receipt_id` INT, `product_id` INT, `quantity_left` INT, `sell_price` INT, `buy_price` INT, `date_receipt` INT);

-- -----------------------------------------------------
-- Placeholder table for view `shop`.`receipt_invoices`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`receipt_invoices` (`id` INT, `date_receipt` INT, `receipt_type` INT, `employee_id` INT, `product_id` INT, `buy_price` INT, `sell_price` INT, `quantity` INT);

-- -----------------------------------------------------
-- Placeholder table for view `shop`.`consumed_invoices`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`consumed_invoices` (`id` INT, `date_consumed` INT, `consumed_type` INT, `is_consumed` INT, `employee_id` INT, `product_id` INT, `receipt_invoice_id` INT, `quantity` INT);

-- -----------------------------------------------------
-- Placeholder table for view `shop`.`consumed_per_receipt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`consumed_per_receipt` (`receipt_invoice_id` INT, `consumed_invoice_id` INT, `product_id` INT, `quantity_consumed` INT);

-- -----------------------------------------------------
-- Placeholder table for view `shop`.`orders_in_progress`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shop`.`orders_in_progress` (`id` INT, `client_id` INT, `date_order` INT, `date_send` INT, `date_payment` INT, `sum` INT, `id_express_invoice` INT, `consumed_invoice_id` INT, `employee_id` INT, `product_id` INT, `quantity` INT, `receipt_invoice_id` INT);

-- -----------------------------------------------------
-- View `shop`.`male_products`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `shop`.`male_products` ;
DROP TABLE IF EXISTS `shop`.`male_products`;
USE `shop`;
CREATE  OR REPLACE VIEW `male_products` AS
SELECT * FROM shop.product WHERE product.gender = 'male';

-- -----------------------------------------------------
-- View `shop`.`female_products`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `shop`.`female_products` ;
DROP TABLE IF EXISTS `shop`.`female_products`;
USE `shop`;
CREATE  OR REPLACE VIEW `female_products` AS
SELECT * FROM shop.product WHERE product.gender = 'female';

-- -----------------------------------------------------
-- View `shop`.`warehouse`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `shop`.`warehouse` ;
DROP TABLE IF EXISTS `shop`.`warehouse`;
USE `shop`;
CREATE  OR REPLACE VIEW `warehouse` AS
    SELECT 
        id AS `receipt_id`,
        shop.receipt_invoices.product_id,
        CASE
            WHEN `consumed_per_receipt`.`quantity_consumed` IS NULL THEN quantity
            ELSE (quantity - `consumed_per_receipt`.`quantity_consumed`)
        END AS `quantity_left`,
        sell_price,
        buy_price,
        date_receipt
    FROM
        shop.receipt_invoices
            LEFT JOIN
        shop.`consumed_per_receipt` ON shop.receipt_invoices.id = `consumed_per_receipt`.receipt_invoice_id
            AND shop.receipt_invoices.product_id = `consumed_per_receipt`.product_id
    WHERE
        CASE
            WHEN `consumed_per_receipt`.`quantity_consumed` IS NULL THEN TRUE
            ELSE (quantity - `consumed_per_receipt`.`quantity_consumed`) > 0
        END
    ORDER BY shop.receipt_invoices.date_receipt ASC;


-- -----------------------------------------------------
-- View `shop`.`receipt_invoices`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `shop`.`receipt_invoices` ;
DROP TABLE IF EXISTS `shop`.`receipt_invoices`;
USE `shop`;
CREATE  OR REPLACE VIEW `receipt_invoices` AS
    SELECT 
        receipt_invoice . *,
        body_receipt_invoice.product_id,
        body_receipt_invoice.buy_price,
        body_receipt_invoice.sell_price,
        body_receipt_invoice.quantity
    FROM
        shop.receipt_invoice
            INNER JOIN
        shop.body_receipt_invoice ON receipt_invoice.id = body_receipt_invoice.receipt_invoice_id; 
;

-- -----------------------------------------------------
-- View `shop`.`consumed_invoices`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `shop`.`consumed_invoices` ;
DROP TABLE IF EXISTS `shop`.`consumed_invoices`;
USE `shop`;
CREATE  OR REPLACE VIEW `consumed_invoices` AS
    SELECT 
        consumed_invoice . *,
        body_consumed_invoice.product_id,
        body_consumed_invoice.receipt_invoice_id,
        body_consumed_invoice.quantity
    FROM
        shop.consumed_invoice
            INNER JOIN
        shop.body_consumed_invoice ON consumed_invoice.id = body_consumed_invoice.consumed_invoice_id; 
;

-- -----------------------------------------------------
-- View `shop`.`consumed_per_receipt`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `shop`.`consumed_per_receipt` ;
DROP TABLE IF EXISTS `shop`.`consumed_per_receipt`;
USE `shop`;
CREATE  OR REPLACE VIEW `consumed_per_receipt` AS
    SELECT 
        receipt_invoice_id,
        id AS `consumed_invoice_id`,
        product_id,
        sum(shop.consumed_invoices.quantity) AS `quantity_consumed`
    FROM
        shop.consumed_invoices
    GROUP BY receipt_invoice_id , product_id
;

-- -----------------------------------------------------
-- View `shop`.`orders_in_progress`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `shop`.`orders_in_progress` ;
DROP TABLE IF EXISTS `shop`.`orders_in_progress`;
USE `shop`;
CREATE  OR REPLACE VIEW `orders_in_progress` AS
    SELECT 
        shop . `order` . *,
        shop.cart.product_id,
        shop.cart.quantity,
        shop.cart.receipt_invoice_id
    FROM
        shop.order
            INNER JOIN
        shop.cart ON order.id = cart.order_id
    WHERE
        shop.`order`.date_send IS NULL
            OR shop.`order`.date_payment IS NULL
            OR shop.`order`.employee_id IS NULL;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
